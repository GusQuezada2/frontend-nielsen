import { Route } from '@angular/router';
import { AuthUnlockSessionComponent } from '@modules/unlock-session/unlock-session.component';

export const authUnlockSessionRoutes: Route[] = [
    {
        path     : '',
        component: AuthUnlockSessionComponent
    }
];
