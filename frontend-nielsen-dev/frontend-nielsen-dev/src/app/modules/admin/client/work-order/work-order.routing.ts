import { Route } from '@angular/router';
import { WorkOrderComponent } from './work-order.component';


export const routes: Route[] = [
    {
        path     : '',
        component: WorkOrderComponent
    }
];
