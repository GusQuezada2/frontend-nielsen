import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WorkOrderComponent } from './work-order.component';
import { RouterModule } from '@angular/router';
import { routes } from './work-order.routing';


@NgModule({
  declarations: [WorkOrderComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),

  ]
})
export class WorkOrderModule { }
