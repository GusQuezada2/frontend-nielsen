import { Component, OnDestroy, OnInit } from '@angular/core';
import { TreoSplashScreenService } from '@treo/services/splash-screen/splash-screen.service';
import { CustomerService } from './customer.service';
import { Subscription } from 'rxjs';
import { Customer } from '@models/customer';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TreoAnimations } from '@treo/animations';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss'],
  providers: [ CustomerService ],
  animations: [ TreoAnimations ]
})
export class CustomerComponent implements OnInit, OnDestroy {
  private subscription: Subscription = new Subscription();
  customers: Customer[] = null;
  loading: boolean = true;
  constructor(private splash : TreoSplashScreenService, 
    private srv : CustomerService, private snack : MatSnackBar) { 
      this.splash.hide();
    }

  ngOnInit(): void {
    this.find();
  }

  private find() {
    this.subscription.add(this.srv.find()
      .subscribe(
        response => {
          this.customers = response;
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.snack.open('Se ha producido un error al intentar conseguir los clientes.', 'X',
            { verticalPosition: 'top', duration: 5000 }
          );
        }
      )
    );
  }
  ngOnDestroy() {
    if (this.subscription) this.subscription.unsubscribe();
  }
}