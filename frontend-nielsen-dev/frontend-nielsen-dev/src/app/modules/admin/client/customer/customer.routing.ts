import { Route } from '@angular/router';
import { CustomerComponent } from './customer.component';
import { CustomerViewComponent } from './customer-view/customer-view.component';


export const routes: Route[] = [
    {
        path     : '',
        component: CustomerComponent
    },
    { path: 'view/:slug',   component: CustomerViewComponent},
];
