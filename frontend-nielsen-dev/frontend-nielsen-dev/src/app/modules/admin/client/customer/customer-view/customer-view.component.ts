import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription} from 'rxjs';
import { UtilService } from '@services/util.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Customer } from '@models/customer';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TreoSplashScreenService } from '@treo/services/splash-screen/splash-screen.service';
import { TreoAnimations } from '@treo/animations';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-customer-view',
  templateUrl: './customer-view.component.html',
  styleUrls: ['./customer-view.component.scss'],
  providers: [ CustomerService ],
  animations: TreoAnimations
})
export class CustomerViewComponent implements OnInit, OnDestroy {
  private subscription :Subscription = new Subscription();
  slug : string;
  screenType: string;
  customerForm: FormGroup;
  loading : boolean = true;
  customer: Customer;
  constructor(private utilSrv : UtilService, private customerSrv : CustomerService,
    private router : Router, private activatedRoute: ActivatedRoute,
    private splash : TreoSplashScreenService, private snack : MatSnackBar, public formBuilder: FormBuilder) {
    this.splash.hide();
  }

  ngOnInit() {
    this.subscription.add(this.activatedRoute.params.subscribe(params => { 
      this.slug = params['slug'];
      this.getCustomer();
    }));

    this.subscription.add(this.utilSrv.screenType$.subscribe(
      screen => { 
        this.screenType = screen;
      }
    ));
  }

  private getCustomer(){
    this.subscription.add(this.customerSrv.findById(this.slug).subscribe(
      response => {
        this.customer = response;
        this.customerForm = this.formBuilder.group({
          slug: this.formBuilder.control({value: response.slug, disabled: true}),
          title: this.formBuilder.control({value: response.title, disabled: true}),
          description: this.formBuilder.control({value: response.description, disabled: true}),
          status: this.formBuilder.control({value: response.status.toString(), disabled: true}),
        });
        this.loading = false;
      }, error => {
        this.loading = false;
        this.snack.open('Se ha producido un error al intentar conseguir el cliente.', 'X',
            { verticalPosition: 'top', duration: 5000 }
        );
      }
    ));
  }

  goBack(){
    this.router.navigate(['/admin/client/customer']);
  }

  ngOnDestroy() {
    if (this.subscription) this.subscription.unsubscribe();
  }

}
