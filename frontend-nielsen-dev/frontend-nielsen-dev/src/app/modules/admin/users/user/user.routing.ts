import { Route } from '@angular/router';
import { UserComponent } from './user.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { UserAddComponent } from './user-add/user-add.component';
import { UserEditComponent } from './user-edit/user-edit.component';


export const routes: Route[] = [
    { path     : '',        component: UserComponent },
    { path: 'view/:rut',   component: UserDetailComponent},
    { path: 'edit/:rut',   component: UserEditComponent},
    { path: 'add',          component: UserAddComponent},
];
