import { Component, OnDestroy, OnInit } from '@angular/core';
import { TreoSplashScreenService } from '@treo/services/splash-screen/splash-screen.service';
import { UserService } from './user.service';
import { Subscription } from 'rxjs';
import { User } from '@models/user';
import { MatSnackBar } from '@angular/material/snack-bar';
import { format } from 'rut.js';
import { ConstantService } from '@services/constant.service';
import { FormControl } from '@angular/forms';
import { TreoAnimations } from '@treo/animations';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
  providers: [ UserService ],
  animations: TreoAnimations
})
export class UserComponent implements OnInit, OnDestroy {
  loading : boolean = true;
  private subscription: Subscription = new Subscription();
  users: User[] = null;
  currentPage:number=0;
  pageSize:number= ConstantService.paginationDesktop;
  totalElements:number;
  searchInputControl: FormControl;

  constructor(private splash : TreoSplashScreenService, 
    private srv : UserService, private snack : MatSnackBar, private router : Router) {
      this.splash.hide();
     }

  ngOnInit(): void {
    this.getCount();
  }

  private getCount(){
    this.subscription.add(this.srv.count()
      .subscribe(
        response => {
          this.totalElements = response.count;
          this.find();
        },
        error => {
          this.loading = false;
          this.snack.open('Se ha producido un error al intentar conseguir los usuarios.', 'X',
            { verticalPosition: 'top', duration: 5000 }
          );
        }
      )
    );
  }

  private find() {
    this.subscription.add(this.srv.find(this.currentPage)
      .subscribe(
        response => {
          this.users = response;
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.snack.open('Se ha producido un error al intentar conseguir los usuarios.', 'X',
            { verticalPosition: 'top', duration: 5000 }
          );
        }
      )
    );
  }

  add(){
    this.router.navigate(['/admin/users/user/add']);
  }

  edit(rut : string){
    this.router.navigate(['/admin/users/user/edit/' + rut ]);
  }

  remove(rut : string){
    this.loading = true;
    this.subscription.add(this.srv.remove(rut)
      .subscribe(
        response => {
          this.find();
          this.snack.open('Se ha eliminado satisfactoriamente el usuario.', 'X',
            { verticalPosition: 'top', duration: 5000 }
          );
        },
        err => {
          this.loading = false;
          this.snack.open(err.error.error.message, 'X',
            { verticalPosition: 'top', duration: 5000 }
          );
        }
      )
    );
  }

  onPageFired(event : any){
    this.currentPage = event.pageIndex;
    this.pageSize = event.pageSize;
    this.find();
  }

  formatRut(rut : string){
    return format(rut);
  }

  ngOnDestroy() {
    if (this.subscription) this.subscription.unsubscribe();
  }
}