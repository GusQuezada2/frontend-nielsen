import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription} from 'rxjs';
import { UtilService } from '@services/util.service';
import { I18nService } from '@services/i18n.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { validate, clean, format } from 'rut.js';
import { UserService } from '../user.service';
import { User } from '@models/user';
import { Role } from '@models/role';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TreoSplashScreenService } from '@treo/services/splash-screen/splash-screen.service';
import { TreoAnimations } from '@treo/animations';
import { Country } from '@models/country';
import { Group } from '@models/group';
import { Customer } from '@models/customer';
import { CountryService } from '@services/country.service';
import { RoleService } from '@modules/admin/users/role/role.service';
import { emailValidator, justLetterValidatorLastAndFirstName,
  selectAnOptionValidator, mobileValidator} from '@shared/utils/form-validators';
import { MatStepper } from '@angular/material/stepper';
import { Department } from '@models/department';
import { Organization } from '@models/organization';
import { DepartmentService } from '@modules/admin/org/department/department.service';
import { CompanyService } from '@modules/admin/org/company/company.service';
import { ChargeService } from '@modules/admin/org/charge/charge.service';
import { GroupService } from '@modules/admin/org/group/group.service';

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.scss'],
  providers: [ UserService, CountryService, RoleService, DepartmentService, CompanyService, ChargeService, GroupService ],
  animations: TreoAnimations
})
export class UserAddComponent implements OnInit, OnDestroy {
  private subscription :Subscription = new Subscription();
  screenType: string;
  countries: Country[] = null;
  roles: Role[] = null;
  step1Form: FormGroup;
  step2Form: FormGroup;
  step3Form: FormGroup;
  originalUser: any;
  updateUser:any;
  user: User = null;
  userAdd: User = null;
  loading : boolean = true;
  loadingSrv : boolean = false;
  departments : Department[];
  companies:Organization[];
  charges:Customer[];
  groups:Group[];
  @ViewChild('stepper') private myStepper: MatStepper;

  constructor(private utilSrv : UtilService, private roleSrv : RoleService, private departmentSrv : DepartmentService,
    private router : Router, public i18n : I18nService, private countrySrv : CountryService,  private companySrv : CompanyService,
    private splash : TreoSplashScreenService, private srv : UserService, private snack : MatSnackBar, public formBuilder: FormBuilder,
    private chargeSrv : ChargeService, private groupSrv : GroupService) {
    this.initForms();
    this.splash.hide();
   }

  ngOnInit() {
    this.subscription.add(this.utilSrv.screenType$.subscribe(
      screen => { 
        this.screenType = screen;
      }
    ));

    this.getCountries();
  }

  private getCountries(){
    this.subscription.add(this.countrySrv.findAll()
      .subscribe(
        response => {
          this.countries = response;
          this.getRoles();
        },
        error => {
          this.loading = false;
          this.snack.open('Se ha producido un error al intentar conseguir los países.', 'X',
            { verticalPosition: 'top', duration: 5000 }
          );
        }
      )
    );
  }

  private getRoles(){
    this.subscription.add(this.roleSrv.findAllActive()
      .subscribe(
        response => {
          this.roles = response;
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.snack.open('Se ha producido un error al intentar conseguir los roles.', 'X',
            { verticalPosition: 'top', duration: 5000 }
          );
        }
      )
    );
  }

  initForms(){

    this.step1Form = this.formBuilder.group({
        rut   : ['', [ Validators.required]],
        name : ['', [ Validators.required, Validators.minLength(3),Validators.maxLength(40), justLetterValidatorLastAndFirstName]],
        lastName: ['', [ Validators.required, Validators.minLength(3),Validators.maxLength(40), justLetterValidatorLastAndFirstName]],
        secondLastName: ['', [Validators.required, Validators.minLength(3),Validators.maxLength(40), justLetterValidatorLastAndFirstName]],
        email: ['', [ Validators.required, Validators.minLength(10), Validators.maxLength(40), emailValidator]],
        phone: ['', [ Validators.minLength(12),Validators.maxLength(12), mobileValidator ]],
        birthdate: [''],
        nationality: [ 'CL', [Validators.required, selectAnOptionValidator ]],
        role : [ null, [ Validators.required, selectAnOptionValidator]]
    });

    this.step2Form = this.formBuilder.group({
      company : [ { value: '', disabled: true}, [ Validators.required, selectAnOptionValidator]],
      department : [ '', [ Validators.required, selectAnOptionValidator]],
      charge : [ '', [ Validators.required, Validators.minLength(3),Validators.maxLength(40), justLetterValidatorLastAndFirstName]],
    });
    this.step3Form = this.formBuilder.group({
      group : [ { value: '', disabled: false}, [ Validators.required, selectAnOptionValidator]],
    });
  }

  onFocusRut(){
    this.step1Form.controls.rut.markAsPristine();
    if (this.step1Form.controls.rut.value != ''){
      this.step1Form.controls.rut.setValue(clean(this.step1Form.controls.rut.value));
    }
  }

  
  onBlurRut() {
    if (this.step1Form.controls.rut.value != '') {
      if (this.step1Form.controls.rut.value.length > 3 && validate(this.step1Form.controls.rut.value)) {
        this.step1Form.controls.rut.setErrors(null);
        this.step1Form.controls.rut.setValue(format(this.step1Form.controls.rut.value));
        
      } else {
        this.step1Form.controls.rut.setErrors({'incorrect': true});
      }
      this.step1Form.controls.rut.markAsDirty();
    }
  }
  
  private save(){
    this.subscription.add(this.srv.add(this.createUser()).subscribe(
      response => {
        this.userAdd = response;
        this.snack.open('Se ha agregado satisfactoriamente el usuario.', 'X',
            { panelClass: ['success'], verticalPosition: 'top', duration: 5000 }
        );
        this.loadingSrv = false;
        this.goForward2(this.myStepper);
      }, error => {
        this.loadingSrv = false;
        this.snack.open('Se ha producido un error al intentar guardar el usuario.', 'X',
            { verticalPosition: 'top', duration: 5000 }
        );
      }
    ));
  }

  add(){
    this.loadingSrv = true;
    this.validateRut();
  }

  private validateEmail(){
    this.subscription.add(this.srv.findByEmail(this.step1Form.controls.email.value).subscribe(
      response => {
        if (response.length === 0){
          this.save();
        } else {
          this.loadingSrv = false;
          this.snack.open('Ya existe un usuario registrado con ese email', 'X',
          { verticalPosition: 'top', duration: 5000 });
        }
      }, error => {
        this.loadingSrv = false;
        this.snack.open('Ya existe un usuario registrado con ese email', 'X',
            { verticalPosition: 'top', duration: 5000 });
      }
    ));
  }

  private validateRut(){
    this.subscription.add(this.srv.findByRut(this.step1Form.controls.rut.value).subscribe(
      response => {
        if (response.length === 0){
          this.validateEmail();
        } else {
          this.loading = false;
          this.snack.open('Ya existe un usuario registrado con ese rut', 'X',
          { verticalPosition: 'top', duration: 5000 });
        }
      }, error => {
        this.loading = false;
        this.snack.open('Ya existe un usuario registrado con ese rut', 'X',
            { verticalPosition: 'top', duration: 5000 });
      }
    ));
  }


  private createUser(){
    let user: any = {
      email: this.step1Form.controls.email.value,
      name: this.step1Form.controls.name.value,
      lastName: this.step1Form.controls.lastName.value,
      secondLastName: this.step1Form.controls.secondLastName.value,
      rut: clean(this.step1Form.controls.rut.value),
      nationality: this.step1Form.controls.nationality.value,
      phone: this.step1Form.controls.phone.value,
      role: this.step1Form.controls.role.value,
      birthdate: this.step1Form.controls.birthdate.value ? this.step1Form.controls.birthdate.value : ""
    }
    return user;
  }

  update(){
    this.loadingSrv = true;
    this.subscription.add(this.srv.update(clean(this.step1Form.controls.rut.value), this.updatedUser()).subscribe(
      response => {
        this.snack.open('Se ha guardado satisfactoriamente la información laboral.', 'X',
            { panelClass: ['success'], verticalPosition: 'top', duration: 5000 }
        );
        this.loadingSrv = false;
        this.goForward3(this.myStepper);
      }, error => {
        this.loadingSrv = false;
        this.snack.open('Se ha producido un error al intentar guardar la información laboral.', 'X',
            { verticalPosition: 'top', duration: 5000 }
        );
      }
    ));
  }

  updateFinal(){
    this.loadingSrv = true;
    this.subscription.add(this.srv.update(clean(this.step1Form.controls.rut.value), this.updatedFinalUser()).subscribe(
      response => {
        this.snack.open('Se ha guardado satisfactoriamente la información de grupos.', 'X',
            { panelClass: ['success'], verticalPosition: 'top', duration: 5000 }
        );
        this.loadingSrv = false;
        this.goBack();
      }, error => {
        this.loadingSrv = false;
        this.snack.open('Se ha producido un error al intentar guardar la información de grupos.', 'X',
            { verticalPosition: 'top', duration: 5000 }
        );
      }
    ));
  }

  private updatedUser(){
    let user: any = {
      email: this.step1Form.controls.email.value,
      name: this.step1Form.controls.name.value,
      lastName: this.step1Form.controls.lastName.value,
      secondLastName: this.step1Form.controls.secondLastName.value,
      rut: clean(this.step1Form.controls.rut.value),
      nationality: this.step1Form.controls.nationality.value,
      phone: this.step1Form.controls.phone.value,
      role: this.step1Form.controls.role.value,
      group: [],
      department: this.step2Form.controls.department.value,
      charge: this.step2Form.controls.charge.value,
      birthdate: this.step1Form.controls.birthdate.value ? this.step1Form.controls.birthdate.value : "",
      status: this.userAdd.status
    }
    return user;
  }

  private updatedFinalUser(){
    let user: any = {
      email: this.step1Form.controls.email.value,
      name: this.step1Form.controls.name.value,
      lastName: this.step1Form.controls.lastName.value,
      secondLastName: this.step1Form.controls.secondLastName.value,
      rut: clean(this.step1Form.controls.rut.value),
      nationality: this.step1Form.controls.nationality.value,
      phone: this.step1Form.controls.phone.value,
      role: this.step1Form.controls.role.value,
      group: this.step3Form.controls.group.value,
      department: this.step2Form.controls.department.value,
      charge: this.step2Form.controls.charge.value,
      birthdate: this.step1Form.controls.birthdate.value ? this.step1Form.controls.birthdate.value : "",
      status: this.userAdd.status
    }
    return user;
  }

  goBack(){
    this.router.navigate(['/admin/users/user']);
  }

  goPrevious(stepper: MatStepper){
    stepper.previous();
}

  goForward2(stepper: MatStepper){
    stepper.next();
    this.getDepartments();
  }
  goForward3(stepper: MatStepper){
    stepper.next();
    this.getGroups();
  }

  private getDepartments(){
    this.subscription.add(this.departmentSrv.findAllActive().subscribe(
      response => {
        this.departments = response;
        this.getCharges();
      }, error => {
        this.snack.open('Se ha producido un error al intentar conseguir los departamentos.', 'X',
            { verticalPosition: 'top', duration: 5000 }
        );
      }
    ));
  }
  private getCharges(){
    this.subscription.add(this.chargeSrv.findAllActive().subscribe(
      response => {
        this.charges = response;
        this.getCompany();
      }, error => {
        this.snack.open('Se ha producido un error al intentar conseguir los cargos.', 'X',
            { verticalPosition: 'top', duration: 5000 }
        );
      }
    ));
  }
  private getCompany(){
    this.subscription.add(this.companySrv.find().subscribe(
      response => {
        this.companies = response;
        this.step2Form.controls.company.setValue(response[0]);
      }, error => {
        this.snack.open('Se ha producido un error al intentar conseguir la compañia.', 'X',
            { verticalPosition: 'top', duration: 5000 }
        );
      }
    ));
  }

  private getGroups(){
    this.subscription.add(this.groupSrv.findAllActive().subscribe(
      response => {
        this.groups = response;
        this.loading = false;
      }, error => {
        this.snack.open('Se ha producido un error al intentar conseguir los grupos.', 'X',
            { verticalPosition: 'top', duration: 5000 }
        );
      }
    ));
  }

  ngOnDestroy() {
    if (this.subscription) this.subscription.unsubscribe();
  }

}