import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AbstractHttpService } from '@services/abstract-http.service';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '@models/user';
import { ConstantService } from '@services/constant.service';
@Injectable()
export class UserService extends AbstractHttpService {

  constructor(protected http: HttpClient) {
    super(http);
  } 

  remove(id: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.delete<any>(`${this.apiUrl}/users/${id}`, httpOptions).pipe(
      map(response => {
        return response;
      })
    );
  }

  find(page : number) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    let skip = 0;
    if (page > 1) {
      skip = ConstantService.paginationDesktop * (page - 1);
    }
    let filter = '{ "skip": "' + skip +'", "limit": "' + ConstantService.paginationDesktop + '", "order" : ["createdAt DESC"]}';
    return this.http
      .get<any>(
        this.apiUrl + '/users?filter=' + encodeURIComponent(filter),
        httpOptions
      )
      .pipe(
        map(response => {
          return response;
        })
      );
  }

  count() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http
      .get<any>(
        this.apiUrl + '/users/count', httpOptions)
      .pipe(
        map(response => {
          return response;
        })
      );
  }

  findByRut(rut: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    const filter =
      '{ "where" : { "rut": "' + rut + '"}}';
    return this.http
      .get<any>(
        this.apiUrl + '/users?filter=' + encodeURIComponent(filter),
        httpOptions
      )
      .pipe(
        map(response => {
          return response;
        })
      );
  }

  findByEmail(email: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    const filter =
      '{ "where" : { "email": "' + email + '"}}';
    return this.http
      .get<any>(
        this.apiUrl + '/users?filter=' + encodeURIComponent(filter),
        httpOptions
      )
      .pipe(
        map(response => {
          return response;
        })
      );
  }

  findById(id: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.get<any>(`${this.apiUrl}/users/${id}`, httpOptions).pipe(
      map(response => {
        return response;
      })
    );
  }
  add(user: User) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post<any>(`${this.apiUrl}/users`, user, httpOptions).pipe(
      map(response => {
        return response;
      })
    );
  }

  update(id: string, user: User) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http
      .put<any>(`${this.apiUrl}/users/${id}`, user, httpOptions)
      .pipe(
        map(response => {
          return response;
        })
      );
  }

  changeStatus(rut: string, status: number) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http
      .put<any>(`${this.apiUrl}/users/status/${rut}/${status}`, {}, httpOptions)
      .pipe(
        map(response => {
          return response;
        })
      );
  }

  uploadImage(file: any) {
    const httpOptions = {
      headers: new HttpHeaders({
        Content: 'multipart/form-data'
      })
    };

    const formData: FormData = new FormData();
    formData.append('file', file, file.name);

    return this.http
      .post<any>(`${this.apiUrl}/fileupload`, formData, httpOptions)
      .pipe(
        map(response => {
          return response;
        })
      );
  }
}
