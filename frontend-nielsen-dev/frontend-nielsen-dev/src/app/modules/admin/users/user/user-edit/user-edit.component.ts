import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { UtilService } from '@services/util.service';
import { I18nService } from '@services/i18n.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { validate, clean, format } from 'rut.js';
import { UserService } from '../user.service';
import { User } from '@models/user';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TreoSplashScreenService } from '@treo/services/splash-screen/splash-screen.service';
import { TreoAnimations } from '@treo/animations';
import { Country } from '@models/country';
import { Role } from '@models/role';
import { Group } from '@models/group';
import { Customer } from '@models/customer';
import { CountryService } from '@services/country.service';
import { RoleService } from '@modules/admin/users/role/role.service';
import { emailValidator, justLetterValidatorLastAndFirstName,
  selectAnOptionValidator, mobileValidator} from '@shared/utils/form-validators';
import { Department } from '@models/department';
import { Organization } from '@models/organization';
import { DepartmentService } from '@modules/admin/org/department/department.service';
import { CompanyService } from '@modules/admin/org/company/company.service';
import { ChargeService } from '@modules/admin/org/charge/charge.service';
import { GroupService } from '@modules/admin/org/group/group.service';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss'],
  providers: [ UserService, CountryService, RoleService, DepartmentService, CompanyService, ChargeService, GroupService ],
  animations: TreoAnimations
})
export class UserEditComponent implements OnInit, OnDestroy {
    private subscription :Subscription = new Subscription();
    screenType: string;
    countries: Country[] = null;
    code: string;
    roles: Role[] = null;
    user: User = null;
    loading : boolean = true;
    step1Form: FormGroup;
    step2Form: FormGroup;
    step3Form: FormGroup;
    statusForm: FormGroup;
    loadingSrv : boolean = false;
    departments : Department[];
    companies:Organization[];
    charges:Customer[];
    groups:Group[];
    constructor(private activatedRoute: ActivatedRoute, private utilSrv : UtilService, private roleSrv : RoleService, private departmentSrv : DepartmentService,
      private router : Router, public i18n : I18nService, private countrySrv : CountryService,  private companySrv : CompanyService,
      private splash : TreoSplashScreenService, private srv : UserService, private snack : MatSnackBar, public formBuilder: FormBuilder,
      private chargeSrv : ChargeService, private groupSrv : GroupService) {
        this.splash.hide();
      }
  
    ngOnInit() {
      this.subscription.add(this.utilSrv.screenType$.subscribe(
        screen => { 
          this.screenType = screen;
        }
      ));
      
      this.subscription.add(this.activatedRoute.params.subscribe(params => { 
        this.code = params['rut'];
        
        this.getCountries();
      }));
    }
  
    private getUser(){
      this.subscription.add(this.srv.findById(this.code).subscribe(
        response => {
          this.user = response;
          this.step1Form = this.formBuilder.group({
            rut   : [format(response.rut), [ Validators.required]],
            name : [response.name, [ Validators.required, Validators.minLength(3),Validators.maxLength(40), justLetterValidatorLastAndFirstName]],
            lastName: [response.lastName, [ Validators.required, Validators.minLength(3),Validators.maxLength(40), justLetterValidatorLastAndFirstName]],
            secondLastName: [response.secondLastName, [Validators.required, Validators.minLength(3),Validators.maxLength(40), justLetterValidatorLastAndFirstName]],
            email: [response.email, [ Validators.required, Validators.minLength(10), Validators.maxLength(40), emailValidator]],
            phone: [response.phone, [ Validators.minLength(12),Validators.maxLength(12), mobileValidator ]],
            birthdate: [response.birthdate],
            nationality: [ response.nationality, [Validators.required, selectAnOptionValidator ]],
            role : [ response.role, [ Validators.required, selectAnOptionValidator]]
          });
          this.step2Form = this.formBuilder.group({
            company : [ { value: this.companies[0], disabled: true}, [ Validators.required, selectAnOptionValidator]],
            department : [ response.department, [ Validators.required, selectAnOptionValidator]],
            charge : [ response.charge, [ Validators.required, Validators.minLength(3),Validators.maxLength(40), justLetterValidatorLastAndFirstName]],
          });
          this.step3Form = this.formBuilder.group({
            group : [ { value: response.group, disabled: false}, [ Validators.required, selectAnOptionValidator]],
          });
          this.statusForm = this.formBuilder.group({
            status: this.formBuilder.control({value: response.status.toString(), disabled: false}, [Validators.required, selectAnOptionValidator]),
          });
          this.loading = false;
        }, error => {
          this.loading = false;
          this.snack.open('Se ha producido un error al intentar conseguir el usuario.', 'X',
            { verticalPosition: 'top', duration: 5000 }
          );
        }
      ));
    }

    private getCountries(){
      this.subscription.add(this.countrySrv.findAll()
        .subscribe(
          response => {
            this.countries = response;
            this.getRoles();
          },
          error => {
            this.loading = false;
            this.snack.open('Se ha producido un error al intentar conseguir los países.', 'X',
              { verticalPosition: 'top', duration: 5000 }
            );
          }
        )
      );
    }
  
    private getRoles(){
      this.subscription.add(this.roleSrv.findAllActive()
        .subscribe(
          response => {
            this.roles = response;
            this.getDepartments();
          },
          error => {
            this.snack.open('Se ha producido un error al intentar conseguir los roles.', 'X',
              { verticalPosition: 'top', duration: 5000 }
            );
          }
        )
      );
    }

    private getDepartments(){
      this.subscription.add(this.departmentSrv.findAllActive().subscribe(
        response => {
          this.departments = response;
          this.getCharges();
        }, error => {
          this.snack.open('Se ha producido un error al intentar conseguir los departamentos.', 'X',
              { verticalPosition: 'top', duration: 5000 }
          );
        }
      ));
    }
    private getCharges(){
      this.subscription.add(this.chargeSrv.findAllActive().subscribe(
        response => {
          this.charges = response;
          this.getCompany();
        }, error => {
          this.snack.open('Se ha producido un error al intentar conseguir los cargos.', 'X',
              { verticalPosition: 'top', duration: 5000 }
          );
        }
      ));
    }
    private getCompany(){
      this.subscription.add(this.companySrv.find().subscribe(
        response => {
          this.companies = response;
          this.getGroups();
        }, error => {
          this.snack.open('Se ha producido un error al intentar conseguir la compañia.', 'X',
              { verticalPosition: 'top', duration: 5000 }
          );
        }
      ));
    }
  
    private getGroups(){
      this.subscription.add(this.groupSrv.findAllActive().subscribe(
        response => {
          this.groups = response;
          this.getUser();
        }, error => {
          this.snack.open('Se ha producido un error al intentar conseguir los grupos.', 'X',
              { verticalPosition: 'top', duration: 5000 }
          );
        }
      ));
    }

    onFocusRut(){
      this.step1Form.controls.rut.markAsPristine();
      if (this.step1Form.controls.rut.value != ''){
        this.step1Form.controls.rut.setValue(clean(this.step1Form.controls.rut.value));
      }
    }
  
    
    onBlurRut() {
      if (this.step1Form.controls.rut.value != '') {
        if (this.step1Form.controls.rut.value.length > 3 && validate(this.step1Form.controls.rut.value)) {
          this.step1Form.controls.rut.setErrors(null);
          this.step1Form.controls.rut.setValue(format(this.step1Form.controls.rut.value));
          
        } else {
          this.step1Form.controls.rut.setErrors({'incorrect': true});
        }
        this.step1Form.controls.rut.markAsDirty();
      }
    }

    changeStatus(){
      this.subscription.add(this.srv.changeStatus(clean(this.step1Form.controls.rut.value), Number.parseInt(this.statusForm.controls.status.value)).subscribe(
        response => {
          this.loading = false;
          this.snack.open('Se ha cambiado el estado satisfactoriamente.', 'X',
              { panelClass: ['success'], verticalPosition: 'top', duration: 5000 }
          );
        }, error => {
          this.loading = false;
          this.snack.open('Se ha producido un error al intentar cambiar el estado.', 'X',
              { verticalPosition: 'top', duration: 5000 }
          );
        }
      ));
    }
    
    private save(){
      this.subscription.add(this.srv.update(clean(this.step1Form.controls.rut.value), this.updatedFinalUser()).subscribe(
        response => {
          this.loading = false;
          this.snack.open('Se ha editado satisfactoriamente el usuario.', 'X',
              { panelClass: ['success'], verticalPosition: 'top', duration: 5000 }
          );
        }, error => {
          this.loading = false;
          this.snack.open('Se ha producido un error al intentar editar el usuario.', 'X',
              { verticalPosition: 'top', duration: 5000 }
          );
        }
      ));
    }

    private updatedFinalUser(){
      let user: any = {
        email: this.step1Form.controls.email.value,
        name: this.step1Form.controls.name.value,
        lastName: this.step1Form.controls.lastName.value,
        secondLastName: this.step1Form.controls.secondLastName.value,
        rut: clean(this.step1Form.controls.rut.value),
        nationality: this.step1Form.controls.nationality.value,
        phone: this.step1Form.controls.phone.value,
        role: this.step1Form.controls.role.value,
        group: this.step3Form.controls.group.value,
        department: this.step2Form.controls.department.value,
        charge: this.step2Form.controls.charge.value,
        birthdate: this.step1Form.controls.birthdate.value ? this.step1Form.controls.birthdate.value : "",
        status: Number.parseInt(this.statusForm.controls.status.value)
      }
      return user;
    }
  
    add(){
      this.loading = true;
      this.validateRut();
    }
  
    private validateEmail(){
      this.subscription.add(this.srv.findByEmail(this.step1Form.controls.email.value).subscribe(
        response => {
          if (response.length === 0 || (response.length === 1 && this.step1Form.controls.email.value === this.user.email)){
            this.save();
          } else {
            this.loading = false;
            this.snack.open('Ya existe un usuario registrado con ese email', 'X',
            { verticalPosition: 'top', duration: 5000 });
          }
        }, error => {
          this.loading = false;
          this.snack.open('Ya existe un usuario registrado con ese email', 'X',
              { verticalPosition: 'top', duration: 5000 });
        }
      ));
    }
  
    private validateRut(){
      this.subscription.add(this.srv.findByRut(clean(this.step1Form.controls.rut.value)).subscribe(
        response => {
          if (response.length === 0 || (response.length === 1 && clean(this.step1Form.controls.rut.value) === this.user.rut)){
            this.validateEmail();
          } else {
            this.loading = false;
            this.snack.open('Ya existe un usuario registrado con ese rut', 'X',
            { verticalPosition: 'top', duration: 5000 });
          }
        }, error => {
          this.loading = false;
          this.snack.open('Ya existe un usuario registrado con ese rut', 'X',
              { verticalPosition: 'top', duration: 5000 });
        }
      ));
    }
  
    goBack(){
      this.router.navigate(['/admin/users/user']);
    }
  
    ngOnDestroy() {
      if (this.subscription) this.subscription.unsubscribe();
    }

  }