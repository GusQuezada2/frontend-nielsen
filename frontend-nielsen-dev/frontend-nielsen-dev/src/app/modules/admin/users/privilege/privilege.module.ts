import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrivilegeComponent } from './privilege.component';
import { RouterModule } from '@angular/router';
import { routes } from './privilege.routing';
import { SharedModule } from 'app/shared/shared.module';
import { TreoCardModule } from '@treo/components/card';
import { TreoMessageModule } from '@treo/components/message';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatPaginatorModule} from '@angular/material/paginator';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatMenuModule } from '@angular/material/menu';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatStepperModule } from '@angular/material/stepper';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { PrivilegeAddComponent } from './privilege-add/privilege-add.component';
import { PrivilegeEditComponent } from './privilege-edit/privilege-edit.component';
import { PrivilegeViewComponent } from './privilege-view/privilege-view.component';



@NgModule({
  declarations: [PrivilegeComponent, PrivilegeAddComponent, PrivilegeEditComponent, PrivilegeViewComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    TreoCardModule,
    TreoMessageModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatButtonModule,
    MatCheckboxModule,
    MatDividerModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatRadioModule,
    MatSelectModule,
    MatStepperModule,
    MatMomentDateModule,
    MatDatepickerModule,
    MatTooltipModule  

  ]
})
export class PrivilegeModule { }
