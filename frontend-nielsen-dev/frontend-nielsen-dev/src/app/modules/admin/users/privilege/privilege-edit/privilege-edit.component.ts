import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { UtilService } from '@services/util.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Role } from '@models/role';
import { Privilege } from '@models/privilege';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TreoSplashScreenService } from '@treo/services/splash-screen/splash-screen.service';
import { TreoAnimations } from '@treo/animations';
import { PrivilegeService } from './../privilege.service';
import { selectAnOptionValidator} from '@shared/utils/form-validators';

@Component({
  selector: 'app-privilege-edit',
  templateUrl: './privilege-edit.component.html',
  styleUrls: ['./privilege-edit.component.scss'],
  providers: [ PrivilegeService ],
  animations: TreoAnimations
})
export class PrivilegeEditComponent implements OnInit, OnDestroy {
  private subscription :Subscription = new Subscription();
  screenType: string;
  privilegeForm: FormGroup;
  loading : boolean = true;
  roles : Role[] = [];
  slug: string;
  privilege : Privilege;
  constructor(private utilSrv : UtilService, private privilegeSrv : PrivilegeService,
    private router : Router, private activatedRoute: ActivatedRoute,
    private splash : TreoSplashScreenService, private snack : MatSnackBar, public formBuilder: FormBuilder) {
    this.splash.hide();
   }

  ngOnInit() {
    this.subscription.add(this.utilSrv.screenType$.subscribe(
      screen => { 
        this.screenType = screen;
      }
    ));
    this.subscription.add(this.activatedRoute.params.subscribe(params => { 
      this.slug = params['slug'];
      this.getPrivilege();
    }));
  }


  private getPrivilege(){
    this.subscription.add(this.privilegeSrv.findById(this.slug)
      .subscribe(
        response => {
          this.privilege = response;
          this.privilegeForm = this.formBuilder.group({
            name: this.formBuilder.control({value: '', disabled: true}),
            page : [response.page, [ Validators.required,selectAnOptionValidator]],
            description : [response.description, [ Validators.required, Validators.minLength(3),Validators.maxLength(5000)]],
            mode : [ response.canRead ? "1" : "2",  [ Validators.required,selectAnOptionValidator]]
          });
          this.setTitle();
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.snack.open('Se ha producido un error al intentar conseguir el privilegio.', 'X',
            { verticalPosition: 'top', duration: 5000 }
          );
        }
      )
    );
  }

  private save(){
    this.subscription.add(this.privilegeSrv.update(this.slug, this.createPrivilege()).subscribe(
      response => {
        this.loading = false;
        this.snack.open('Se ha editado satisfactoriamente el privilegio.', 'X',
            { panelClass: ['success'], verticalPosition: 'top', duration: 5000 }
        );
      }, error => {
        this.loading = false;
        this.snack.open('Se ha producido un error al intentar editar el privilegio.', 'X',
            { verticalPosition: 'top', duration: 5000 }
        );
      }
    ));
  }

  add(){
    this.loading = true;
    this.subscription.add(this.privilegeSrv.findByName(this.privilegeForm.controls.name.value).subscribe(
      response => {
        if (response.length === 0 || (response.length === 1 && this.privilegeForm.controls.name.value === this.privilege.title)){
          this.save();
        } else {
          this.loading = false;
          this.snack.open('Ya existe un privilegio con ese nombre', 'X',
          { verticalPosition: 'top', duration: 5000 });
        }
      }, error => {
        this.loading = false;
        this.snack.open('Ya existe un privilegio con ese nombre', 'X',
            { verticalPosition: 'top', duration: 5000 });
      }
    ));
  }

  setTitle(){
    let preffix = "";
    let suffix = "";
    if (this.privilegeForm.controls.mode.value !== ''){
      suffix = this.privilegeForm.controls.mode.value === "1" ? "Lectura" : "Escritura";
    }
    if (this.privilegeForm.controls.page.value !== ''){
      preffix = this.privilegeForm.controls.page.value
    }

    this.privilegeForm.controls.name.setValue(preffix + " " + suffix);
  }

  private createPrivilege(){
    let privilege = {
      page: this.privilegeForm.controls.page.value,
      description: this.privilegeForm.controls.description.value,
      canWrite: this.privilegeForm.controls.mode.value === "2" ? true : false,
      canRead: this.privilegeForm.controls.mode.value === "1" ? true : false,
      title: this.privilegeForm.controls.name.value
    }
    return privilege;
  }

  goBack(){
    this.router.navigate(['/admin/users/privilege']);
  }
  ngOnDestroy() {
    if (this.subscription) this.subscription.unsubscribe();
  }
}