import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription} from 'rxjs';
import { UtilService } from '@services/util.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TreoSplashScreenService } from '@treo/services/splash-screen/splash-screen.service';
import { TreoAnimations } from '@treo/animations';
import { PrivilegeService } from './../privilege.service';

@Component({
  selector: 'app-privilege-view',
  templateUrl: './privilege-view.component.html',
  styleUrls: ['./privilege-view.component.scss'],
  providers: [ PrivilegeService ],
  animations: TreoAnimations
})
export class PrivilegeViewComponent implements OnInit, OnDestroy {
  private subscription :Subscription = new Subscription();
  slug : string;
  screenType: string;
  privilegeForm: FormGroup;
  loading : boolean = true;
  constructor(private utilSrv : UtilService, private roleSrv : PrivilegeService,
    private router : Router, private activatedRoute: ActivatedRoute, 
    private splash : TreoSplashScreenService, private snack : MatSnackBar, public formBuilder: FormBuilder) {
    this.splash.hide();
  }

  ngOnInit() {
    this.subscription.add(this.activatedRoute.params.subscribe(params => { 
      this.slug = params['slug'];
      this.getPrivilege();
    }));

    this.subscription.add(this.utilSrv.screenType$.subscribe(
      screen => { 
        this.screenType = screen;
      }
    ));
  }

  private getPrivilege(){
    this.subscription.add(this.roleSrv.findById(this.slug)
      .subscribe(
        response => {
          this.privilegeForm = this.formBuilder.group({
            slug: this.formBuilder.control({value: response.slug, disabled: true}),            
            createdAt: this.formBuilder.control({value: response.createdAt, disabled: true}),
            createdBy: this.formBuilder.control({value: response.createdBy, disabled: true}),
            name: this.formBuilder.control({value: response.title, disabled: true}),
            description: this.formBuilder.control({value: response.description, disabled: true}),
            page: this.formBuilder.control({value: response.page, disabled: true}),
            mode: this.formBuilder.control({value: response.canRead ? "1" : "2", disabled: true}),
          });
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.snack.open('Se ha producido un error al intentar conseguir los roles.', 'X',
            { verticalPosition: 'top', duration: 5000 }
          );
        }
      )
    );
  }

  goBack(){
    this.router.navigate(['/admin/users/privilege']);
  }

  ngOnDestroy() {
    if (this.subscription) this.subscription.unsubscribe();
  }

}
