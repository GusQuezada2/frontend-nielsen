import { Route } from '@angular/router';
import { PrivilegeComponent } from './privilege.component';
import { PrivilegeAddComponent} from './privilege-add/privilege-add.component';
import { PrivilegeEditComponent} from './privilege-edit/privilege-edit.component';
import { PrivilegeViewComponent} from './privilege-view/privilege-view.component';

export const routes: Route[] = [
    {
        path     : '',
        component: PrivilegeComponent
    },
    { path: 'add',          component: PrivilegeAddComponent},
    { path: 'view/:slug',   component: PrivilegeViewComponent},
    { path: 'edit/:slug',   component: PrivilegeEditComponent},
];
