import { Component, OnDestroy, OnInit } from '@angular/core';
import { TreoSplashScreenService } from '@treo/services/splash-screen/splash-screen.service';
import { PrivilegeService } from './privilege.service';
import { Subscription } from 'rxjs';
import { Privilege } from '@models/privilege';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormControl } from '@angular/forms';
import { ConstantService } from '@services/constant.service';
import { TreoAnimations } from '@treo/animations';
import { Router } from '@angular/router';
@Component({
  selector: 'app-privilege',
  templateUrl: './privilege.component.html',
  styleUrls: ['./privilege.component.scss'],
  providers: [ PrivilegeService],
  animations : [ TreoAnimations ]
})
export class PrivilegeComponent implements OnInit, OnDestroy {
  private subscription: Subscription = new Subscription();
  privileges: Privilege[] = null;
  loading : boolean = true;
  currentPage:number=0;
  pageSize:number= ConstantService.paginationDesktop;
  totalElements:number;
  searchInputControl: FormControl;
  constructor(private splash : TreoSplashScreenService, private router : Router, 
    private srv : PrivilegeService, private snack : MatSnackBar) { 
      this.splash.hide();
    }

  ngOnInit(): void {
    this.getCount();
  }

  private getCount(){
    this.subscription.add(this.srv.count()
      .subscribe(
        response => {
          this.totalElements = response.count;
          this.find();
        },
        error => {
          this.loading = false;
          this.snack.open('Se ha producido un error al intentar conseguir los privilegios.', 'X',
            { verticalPosition: 'top', duration: 5000 }
          );
        }
      )
    );
  }

  private find() {
    this.subscription.add(this.srv.find(this.currentPage)
      .subscribe(
        response => {
          this.privileges = response;
          window.scrollTo(0, 0);
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.snack.open('Se ha producido un error al intentar conseguir los privilegios.', 'X',
            { verticalPosition: 'top', duration: 5000 }
          );
        }
      )
    );
  }

  edit(slug : string){
    this.router.navigate(['/admin/users/privilege/edit/' + slug ]);
  }

  remove(slug : string){
    this.loading = true;
    this.subscription.add(this.srv.remove(slug)
      .subscribe(
        response => {
          this.find();
          this.snack.open('Se ha eliminado satisfactoriamente el privilegio.', 'X',
            { verticalPosition: 'top', duration: 5000 }
          );
        },
        err => {
          this.loading = false;
          this.snack.open(err.error.error.message, 'X',
            { verticalPosition: 'top', duration: 5000 }
          );
        }
      )
    );
  }

  onPageFired(event : any){
    this.currentPage = event.pageIndex;
    this.pageSize = event.pageSize;
    this.loading = true;
    this.find();
  }
  add(){
    this.router.navigate(['/admin/users/privilege/add']);
  }
  ngOnDestroy() {
    if (this.subscription) this.subscription.unsubscribe();
  }
}