import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UtilService } from '@services/util.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Role } from '@models/role';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TreoSplashScreenService } from '@treo/services/splash-screen/splash-screen.service';
import { TreoAnimations } from '@treo/animations';
import { PrivilegeService } from './../privilege.service';
import { RoleService } from '@modules/admin/users/role/role.service';
import { selectAnOptionValidator} from '@shared/utils/form-validators';
@Component({
  selector: 'app-privilege-add',
  templateUrl: './privilege-add.component.html',
  styleUrls: ['./privilege-add.component.scss'],
  providers: [ PrivilegeService, RoleService ],
  animations: TreoAnimations
})
export class PrivilegeAddComponent implements OnInit, OnDestroy {
  private subscription :Subscription = new Subscription();
  screenType: string;
  privilegeForm: FormGroup;
  loading : boolean = false;
  roles : Role[] = [];
  constructor(private utilSrv : UtilService, private privilegeSrv : PrivilegeService,
    private router : Router, private roleSrv : RoleService,
    private splash : TreoSplashScreenService, private snack : MatSnackBar, public formBuilder: FormBuilder) {
    this.initForms();
    this.splash.hide();
   }

  ngOnInit() {
    this.subscription.add(this.utilSrv.screenType$.subscribe(
      screen => { 
        this.screenType = screen;
      }
    ));
  }


  initForms(){

    this.privilegeForm = this.formBuilder.group({
      name: this.formBuilder.control({value: '', disabled: true}),
      page : ['', [ Validators.required,selectAnOptionValidator]],
      description : ['', [ Validators.required, Validators.minLength(3),Validators.maxLength(5000)]],
      mode : [ '',  [ Validators.required,selectAnOptionValidator]]
    });
  }

  private save(){
    this.subscription.add(this.privilegeSrv.add(this.createPrivilege()).subscribe(
      response => {
        this.loading = false;
        this.snack.open('Se ha agregado satisfactoriamente el privilegio.', 'X',
            { panelClass: ['success'], verticalPosition: 'top', duration: 5000 }
        );
        this.goBack();
      }, error => {
        this.loading = false;
        this.snack.open('Se ha producido un error al intentar guardar el privilegio.', 'X',
            { verticalPosition: 'top', duration: 5000 }
        );
      }
    ));
  }

  add(){
    this.loading = true;
    this.subscription.add(this.privilegeSrv.findByName(this.privilegeForm.controls.name.value).subscribe(
      response => {
        if (response.length === 0){
          this.save();
        } else {
          this.loading = false;
          this.snack.open('Ya existe el privilegio', 'X',
          { verticalPosition: 'top', duration: 5000 });
        }
      }, error => {
        this.loading = false;
        this.snack.open('Ya existe un perfil con ese nombre', 'X',
            { verticalPosition: 'top', duration: 5000 });
      }
    ));
  }

  setTitle(){
    let preffix = "";
    let suffix = "";
    if (this.privilegeForm.controls.mode.value !== ''){
      suffix = this.privilegeForm.controls.mode.value === "1" ? "Lectura" : "Escritura";
    }
    if (this.privilegeForm.controls.page.value !== ''){
      preffix = this.privilegeForm.controls.page.value
    }

    this.privilegeForm.controls.name.setValue(preffix + " " + suffix);
  }

  private createPrivilege(){
    let privilege = {
      page: this.privilegeForm.controls.page.value,
      description: this.privilegeForm.controls.description.value,
      canWrite: this.privilegeForm.controls.mode.value === "2" ? true : false,
      canRead: this.privilegeForm.controls.mode.value === "1" ? true : false,
      title: this.privilegeForm.controls.name.value
    }
    return privilege;
  }

  goBack(){
    this.router.navigate(['/admin/users/privilege']);
  }
  ngOnDestroy() {
    if (this.subscription) this.subscription.unsubscribe();
  }
}
