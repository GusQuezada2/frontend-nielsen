import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription} from 'rxjs';
import { UtilService } from '@services/util.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Role } from '@models/role';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TreoSplashScreenService } from '@treo/services/splash-screen/splash-screen.service';
import { TreoAnimations } from '@treo/animations';
import { RoleService } from './../role.service';
import { Privilege } from '@models/privilege';
import { PrivilegeService } from '@modules/admin/users/privilege/privilege.service';

@Component({
  selector: 'app-role-view',
  templateUrl: './role-view.component.html',
  styleUrls: ['./role-view.component.scss'],
  providers: [ RoleService, PrivilegeService ],
  animations: TreoAnimations
})
export class RoleViewComponent implements OnInit, OnDestroy {
  private subscription :Subscription = new Subscription();
  slug : string;
  screenType: string;
  roleForm: FormGroup;
  privilegeForm: FormGroup;
  loading : boolean = true;
  privileges: Privilege[] = null;
  constructor(private utilSrv : UtilService, private roleSrv : RoleService,
    private router : Router, private activatedRoute: ActivatedRoute, private privilegeSrv : PrivilegeService,
    private splash : TreoSplashScreenService, private snack : MatSnackBar, public formBuilder: FormBuilder) {
    this.splash.hide();
  }

  ngOnInit() {
    this.subscription.add(this.activatedRoute.params.subscribe(params => { 
      this.slug = params['slug'];
      this.getPrivileges();
    }));

    this.subscription.add(this.utilSrv.screenType$.subscribe(
      screen => { 
        this.screenType = screen;
      }
    ));
  }

  private getPrivileges(){
    this.subscription.add(this.privilegeSrv.findAll().subscribe(
      response => {
        this.privileges = response;
        this.getRole();
      }, error => {
        this.loading = false;
        this.snack.open('Se ha producido un error al intentar conseguir los permisos.', 'X',
            { verticalPosition: 'top', duration: 5000 }
        );
      }
    ));
  }

  private getRole(){
    this.subscription.add(this.roleSrv.findById(this.slug)
      .subscribe(
        response => {
          this.roleForm = this.formBuilder.group({
            slug: this.formBuilder.control({value: response.slug, disabled: true}),
            title: this.formBuilder.control({value: response.title, disabled: true}),
            description: this.formBuilder.control({value: response.description, disabled: true}),
            createdAt: this.formBuilder.control({value: response.createdAt, disabled: true}),
            createdBy: this.formBuilder.control({value: response.createdBy, disabled: true}),
            status: this.formBuilder.control({value: response.status.toString(), disabled: true}),
            privileges: this.formBuilder.control({value: response.privilege, disabled: true})
          });
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.snack.open('Se ha producido un error al intentar conseguir los roles.', 'X',
            { verticalPosition: 'top', duration: 5000 }
          );
        }
      )
    );
  }

  goBack(){
    this.router.navigate(['/admin/users/role']);
  }

  ngOnDestroy() {
    if (this.subscription) this.subscription.unsubscribe();
  }

}
