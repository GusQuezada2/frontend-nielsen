import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UtilService } from '@services/util.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TreoSplashScreenService } from '@treo/services/splash-screen/splash-screen.service';
import { TreoAnimations } from '@treo/animations';
import { RoleService } from './../role.service';
import { PrivilegeService } from '@modules/admin/users/privilege/privilege.service';
import { Privilege } from '@models/privilege';
import { checkedOptionValidator} from '@shared/utils/form-validators';

@Component({
  selector: 'app-role-add',
  templateUrl: './role-add.component.html',
  styleUrls: ['./role-add.component.scss'],
  providers: [ RoleService, PrivilegeService ],
  animations: TreoAnimations
})
export class RoleAddComponent implements OnInit, OnDestroy {
  private subscription :Subscription = new Subscription();
  screenType: string;
  roleForm: FormGroup;
  loading : boolean = true;
  privileges: Privilege[] = null;
  constructor(private utilSrv : UtilService, private roleSrv : RoleService,
    private router : Router, private privilegeSrv : PrivilegeService,
    private splash : TreoSplashScreenService, private snack : MatSnackBar, public formBuilder: FormBuilder) {
    this.initForms();
    this.splash.hide();
   }

  ngOnInit() {
    this.subscription.add(this.utilSrv.screenType$.subscribe(
      screen => { 
        this.screenType = screen;
      }
    ));
  }
  initForms(){

    this.roleForm = this.formBuilder.group({
      title : ['', [ Validators.required, Validators.minLength(3),Validators.maxLength(60)]],
      description : ['', [ Validators.required, Validators.minLength(3),Validators.maxLength(5000)]],
      status : ['', [ Validators.required, checkedOptionValidator]],
      privileges : ['', [ Validators.required, checkedOptionValidator]]
    });

    this.getPrivileges();
  }

  private getPrivileges(){
    this.subscription.add(this.privilegeSrv.findAll().subscribe(
      response => {
        this.privileges = response;
        this.loading = false;
      }, error => {
        this.loading = false;
        this.snack.open('Se ha producido un error al intentar conseguir los permisos.', 'X',
            { verticalPosition: 'top', duration: 5000 }
        );
      }
    ));
  }


  private save(){
    this.subscription.add(this.roleSrv.add(this.createRole()).subscribe(
      response => {
        this.loading = false;
        this.snack.open('Se ha agregado satisfactoriamente el perfil.', 'X',
            { panelClass: ['success'], verticalPosition: 'top', duration: 5000 }
        );
        this.goBack();
      }, error => {
        this.loading = false;
        this.snack.open('Se ha producido un error al intentar guardar el perfil.', 'X',
            { verticalPosition: 'top', duration: 5000 }
        );
      }
    ));
  }

  add(){
    this.loading = true;
    this.subscription.add(this.roleSrv.findByName(this.roleForm.controls.title.value).subscribe(
      response => {
        if (response.length === 0){
          this.save();
        } else {
          this.loading = false;
          this.snack.open('Ya existe un perfil con ese nombre', 'X',
          { verticalPosition: 'top', duration: 5000 });
        }
      }, error => {
        this.loading = false;
        this.snack.open('Ya existe un perfil con ese nombre', 'X',
            { verticalPosition: 'top', duration: 5000 });
      }
    ));
  }

  private createRole(){
    let role = {
      status: Number.parseInt(this.roleForm.controls.status.value),
      title: this.roleForm.controls.title.value,
      description: this.roleForm.controls.description.value,
      privilege: this.roleForm.controls.privileges.value
    }
    return role;
  }

  goBack(){
    this.router.navigate(['/admin/users/role']);
  }
  ngOnDestroy() {
    if (this.subscription) this.subscription.unsubscribe();
  }
}
