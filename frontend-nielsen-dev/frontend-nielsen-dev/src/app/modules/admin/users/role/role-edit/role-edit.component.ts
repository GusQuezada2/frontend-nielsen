import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription} from 'rxjs';
import { UtilService } from '@services/util.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Role } from '@models/role';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TreoSplashScreenService } from '@treo/services/splash-screen/splash-screen.service';
import { TreoAnimations } from '@treo/animations';
import { RoleService } from './../role.service';
import { Privilege } from '@models/privilege';
import { PrivilegeService } from '@modules/admin/users/privilege/privilege.service';

@Component({
  selector: 'app-role-edit',
  templateUrl: './role-edit.component.html',
  styleUrls: ['./role-edit.component.scss'],
  providers: [ RoleService, PrivilegeService ],
  animations: TreoAnimations
})
export class RoleEditComponent implements OnInit, OnDestroy {
  private subscription :Subscription = new Subscription();
  slug : string;
  screenType: string;
  roleForm: FormGroup;
  privilegeForm: FormGroup;
  loading : boolean = true;
  role : Role;
  privileges: Privilege[] = null;
  constructor(private utilSrv : UtilService, private roleSrv : RoleService,
    private activatedRoute: ActivatedRoute, private router : Router, private privilegeSrv : PrivilegeService,
    private splash : TreoSplashScreenService, private snack : MatSnackBar, public formBuilder: FormBuilder) {
    this.splash.hide();
  }

  ngOnInit() {
    this.subscription.add(this.activatedRoute.params.subscribe(params => { 
      this.slug = params['slug'];
      this.getPrivileges();
    }));

    this.subscription.add(this.utilSrv.screenType$.subscribe(
      screen => { 
        this.screenType = screen;
      }
    ));
  }

  private getPrivileges(){
    this.subscription.add(this.privilegeSrv.findAll().subscribe(
      response => {
        this.privileges = response;
        this.getRole();
      }, error => {
        this.loading = false;
        this.snack.open('Se ha producido un error al intentar conseguir los permisos.', 'X',
            { verticalPosition: 'top', duration: 5000 }
        );
      }
    ));
  }

  private getRole(){
    this.subscription.add(this.roleSrv.findById(this.slug)
      .subscribe(
        response => {
          this.role = response;
          this.roleForm = this.formBuilder.group({
            title: this.formBuilder.control({value: response.title, disabled: false}),
            description: this.formBuilder.control({value: response.description, disabled: false}),
            status: this.formBuilder.control({value: response.status.toString(), disabled: false}),
            privileges: this.formBuilder.control({value: response.privilege, disabled: false}),
          });
          this.loading = false;
        },
        error => {
          this.snack.open('Se ha producido un error al intentar conseguir los roles.', 'X',
            { verticalPosition: 'top', duration: 5000 }
          );
        }
      )
    );
  }

  private save(){
    this.subscription.add(this.roleSrv.update(this.slug, this.createRole()).subscribe(
      response => {
        this.loading = false;
        this.snack.open('Se ha editado satisfactoriamente el perfil.', 'X',
            { panelClass: ['success'], verticalPosition: 'top', duration: 5000 }
        );
      }, error => {
        this.loading = false;
        this.snack.open('Se ha producido un error al intentar editar el perfil.', 'X',
            { verticalPosition: 'top', duration: 5000 }
        );
      }
    ));
  }

  add(){
    this.loading = true;
    this.subscription.add(this.roleSrv.findByName(this.roleForm.controls.title.value).subscribe(
      response => {
        if (response.length === 0 || (response.length === 1 && this.roleForm.controls.title.value === this.role.title)){
         this.save();
        } else {
          this.loading = false;
          this.snack.open('Ya existe un perfil con ese nombre', 'X',
          { verticalPosition: 'top', duration: 5000 });
        }
      }, error => {
        this.loading = false;
        this.snack.open('Ya existe un perfil con ese nombre', 'X',
            { verticalPosition: 'top', duration: 5000 });
      }
    ));
  }

  private createRole(){
    let role = {
      status: Number.parseInt(this.roleForm.controls.status.value),
      title: this.roleForm.controls.title.value,
      description: this.roleForm.controls.description.value,
      privilege: this.roleForm.controls.privileges.value

    }
    return role;
  }
  goBack(){
    this.router.navigate(['/admin/users/role']);
  }
  ngOnDestroy() {
    if (this.subscription) this.subscription.unsubscribe();
  }

}
