import { Component, OnInit, OnDestroy } from '@angular/core';
import { TreoSplashScreenService } from '@treo/services/splash-screen/splash-screen.service';
import { RoleService } from './role.service';
import { Subscription } from 'rxjs';
import { Role } from '@models/role';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormControl } from '@angular/forms';
import { ConstantService } from '@services/constant.service';
import { TreoAnimations } from '@treo/animations';
import { Router } from '@angular/router';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.scss'],
  providers: [ RoleService],
  animations : [ TreoAnimations ]
})
export class RoleComponent implements OnInit, OnDestroy {
  private subscription: Subscription = new Subscription();
  roles: Role[] = null;
  loading : boolean = true;
  currentPage:number=0;
  pageSize:number= ConstantService.paginationDesktop;
  totalElements:number;
  searchInputControl: FormControl;
  constructor(private splash : TreoSplashScreenService, private router : Router, 
    private srv : RoleService, private snack : MatSnackBar) { 
      this.splash.hide();
    }

  ngOnInit(): void {
    this.getCount();
  }

  private getCount(){
    this.subscription.add(this.srv.count()
      .subscribe(
        response => {
          this.totalElements = response.count;
          this.find();
        },
        error => {
          this.loading = false;
          this.snack.open('Se ha producido un error al intentar conseguir los perfiles.', 'X',
            { verticalPosition: 'top', duration: 5000 }
          );
        }
      )
    );
  }

  private find() {
    this.subscription.add(this.srv.find(this.currentPage)
      .subscribe(
        response => {
          this.roles = response;
          window.scrollTo(0, 0);
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.snack.open('Se ha producido un error al intentar conseguir los perfiles.', 'X',
            { verticalPosition: 'top', duration: 5000 }
          );
        }
      )
    );
  }

  edit(slug : string){
    this.router.navigate(['/admin/users/role/edit/' + slug ]);
  }

  remove(slug : string){
    this.loading = true;
    this.subscription.add(this.srv.remove(slug)
      .subscribe(
        response => {
          this.find();
          this.snack.open('Se ha eliminado satisfactoriamente el perfil.', 'X',
            { verticalPosition: 'top', duration: 5000 }
          );
        },
        err => {
          this.loading = false;
          this.snack.open(err.error.error.message, 'X',
            { verticalPosition: 'top', duration: 5000 }
          );
        }
      )
    );
  }

  onPageFired(event : any){
    this.currentPage = event.pageIndex;
    this.pageSize = event.pageSize;
    this.loading = true;
    this.find();
  }
  add(){
    this.router.navigate(['/admin/users/role/add']);
  }
  ngOnDestroy() {
    if (this.subscription) this.subscription.unsubscribe();
  }
}
