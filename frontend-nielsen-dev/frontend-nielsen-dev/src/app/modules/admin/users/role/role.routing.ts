import { Route } from '@angular/router';
import { RoleComponent } from './role.component';
import { RoleAddComponent } from './role-add/role-add.component';
import { RoleViewComponent } from './role-view/role-view.component';
import { RoleEditComponent } from './role-edit/role-edit.component';


export const routes: Route[] = [
    { path     : '', component: RoleComponent },
    { path: 'add',          component: RoleAddComponent},
    { path: 'view/:slug',   component: RoleViewComponent},
    { path: 'edit/:slug',   component: RoleEditComponent},
];
