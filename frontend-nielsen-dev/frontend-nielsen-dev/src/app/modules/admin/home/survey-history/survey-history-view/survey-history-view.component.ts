import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription} from 'rxjs';
import { UtilService } from '@services/util.service';
import { I18nService } from '@services/i18n.service';
import { FormBuilder, FormGroup, Validators , FormControl} from '@angular/forms';
import { FormService } from '@modules/admin/org/form/form.service';
import { Form } from '@models/form';
import { Role } from '@models/role';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TreoSplashScreenService } from '@treo/services/splash-screen/splash-screen.service';
import { TreoAnimations } from '@treo/animations';
import { Group } from '@models/group';
import { Customer } from '@models/customer';
import { CustomerService } from '@modules/admin/client/customer/customer.service';
import { GroupService } from '@modules/admin/org/group/group.service';
import { MySurveysService } from '@modules/admin/home/my-surveys/my-surveys.service';

@Component({
  selector: 'app-survey-history-view',
  templateUrl: './survey-history-view.component.html',
  styleUrls: ['./survey-history-view.component.scss'],
  providers: [ FormService, GroupService, CustomerService, MySurveysService ],
  animations: TreoAnimations
})
export class SurveyHistoryViewComponent implements OnInit, OnDestroy {
  private subscription :Subscription = new Subscription();
  screenType: string;
  customers: Customer[] = null;
  roles: Role[] = null;
  step1Form: FormGroup;
  step2Form: FormGroup;
  statusForm: FormGroup;
  form: Form = null;
  formAdd: Form = null;
  loading : boolean = true;
  loadingSrv : boolean = false;
  groups:Group[];
  relation:boolean = false;
  code: string;
  questionsList = [];
  namePoll: string = "";
  error: boolean = false;
  mySurvey : any = null;
  geo = {
    latitude: 0,
    longitude: 0
  }

    constructor(private activatedRoute: ActivatedRoute, private utilSrv : UtilService, private srv : FormService,
      private router : Router, public i18n : I18nService, private customerSrv : CustomerService,
      private splash : TreoSplashScreenService, private snack : MatSnackBar, public formBuilder: FormBuilder,
      private groupSrv : GroupService, private mySurveySrv : MySurveysService) {
      this.splash.hide();
     }
  
    ngOnInit() {
      this.subscription.add(this.utilSrv.screenType$.subscribe(
        screen => { 
          this.screenType = screen;
        }
      ));
      this.subscription.add(this.activatedRoute.params.subscribe(params => { 
        this.code = params['slug'];
        this.getGroups();
      }));
    }
  
    private getGroups(){
      this.subscription.add(this.groupSrv.findAllActive().subscribe(
        response => {
          this.groups = response;
          this.getCustomers()
        }, error => {
          this.loading = false;

          this.snack.open('Se ha producido un error al intentar conseguir los grupos.', 'X',
              { verticalPosition: 'top', duration: 5000 }
          );
        }
      ));
    }
    private getCustomers(){
      this.subscription.add(this.customerSrv.findAllActive().subscribe(
        response => {
          this.customers = response;
          this.getForm();
        }, error => {
          this.loading = false;
          this.snack.open('Se ha producido un error al intentar conseguir los clientes.', 'X',
              { verticalPosition: 'top', duration: 5000 }
          );
        }
      ));
    }
  
    getForm(){
      this.subscription.add(this.srv.findById(this.code).subscribe(
        response => {
          this.form = response;
          this.namePoll = response.title;
          let mode = response.group ? 'Grupo' : 'Orden de Trabajo';

          this.step1Form = this.formBuilder.group({
            title : [ { value : response.title, disabled: true}],
            description : [{ value : response.description, disabled: true}],
            customer : [ { value : response.customer, disabled: true}],
            mode:[ { value : response.group ? '1' : '2', disabled: true}],
            modeName:[ { value : mode, disabled: true}],
            group : [ { value : response.group ? response.group : null, disabled: true}],
            ot : [ { value : response.ot ? response.ot : null , disabled: true}],
            createdAt:[ {value: response.publishAt, disabled: true}],
            createdBy: [{value: response.createdBy, disabled: true}],
            slug: [{value: response.slug, disabled: true}],
            status: this.formBuilder.control({value: response.status.toString(), disabled: true}),
            vigency: [{value: response.vigencyAt === null || response.vigencyAt.substring(0, 10) === '5000-01-01' ? 'Indenfinida' : 'Definida', disabled: true}],
            vigencyDate: [{value:  response.vigencyAt === null || response.vigencyAt.substring(0, 10) === '5000-01-01' ? '' : response.vigencyAt, disabled: true}]
          });
          this.checkMySurvey();
        }, error => {
          this.loading = false;
          this.snack.open('Se ha producido un error al intentar conseguir el formulario.', 'X',
              { verticalPosition: 'top', duration: 5000 }
          );
        }
      ));
    } 

    checkMySurvey(){
      this.subscription.add(this.mySurveySrv.findByForm(this.form.slug).subscribe(
        response => {
          if (response.length > 0){
            this.mySurvey = response[0];
            this.populateFull();
            this.loading = false;
          } else {
            this.loading = false;
            this.snack.open('Se ha producido un error al intentar recuperar las respuestas.', 'X',
              { verticalPosition: 'top', duration: 5000 }
          );
          }
          
        }, error => {
          this.loading = false;
          this.snack.open('Se ha producido un error al intentar recuperar las respuestas.', 'X',
              { verticalPosition: 'top', duration: 5000 }
          );
        }
      ));
    }
   

    private populateFull(){
      this.step2Form = this.formBuilder.group({});
      if (this.form.questions){
        for (let i = 0; i < this.form.questions.length; i++){
          this.questionsList.push({ title: this.form.questions[i].title, answer : []})

          let selected = null; 
          for (let j = 0; j < this.mySurvey.questions[i].answer.length; j++){
            if (this.mySurvey.questions[i].answer[j].selected){
              selected = j.toString();
              break;
            }
          }

          this.step2Form.addControl('question' + (this.questionsList.length - 1),
            new FormControl({ value: selected, disabled: this.mySurvey.status === 0 ? false : true}, [ Validators.required])
          );

          for (let j = 0; j < this.form.questions[i].alternatives.length; j++){
            this.questionsList[i].answer.push( { value: this.form.questions[i].alternatives[j], selected : false });
          }
        }
      }
    }

    download() {
      this.loadingSrv = true;
      this.mySurveySrv.getPdfB64(this.mySurvey.id).subscribe(
        data => {
          const date = new Date();
          const linkSource = `data:application/pdf;base64,${data.file}`;
          const downloadLink = document.createElement('a');
          const fileName = this.form.createdBy + "_" + this.form.title + '.pdf';
  
          downloadLink.href = linkSource;
          downloadLink.download = fileName;
          document.body.appendChild(downloadLink);
          downloadLink.click();
          this.loadingSrv = false;
          this.snack.open(
            'Se ha descargado satisfactoriamente el archivo. Revise sus descargas.',
            'Cerrar',
            { panelClass: ['success'], verticalPosition: 'top', duration: 3000 }
          );
        },
        error => {
          this.loadingSrv = false;
          this.snack.open(
            'Hay problemas en la generación del pdf. Intente más tarde.',
            'Cerrar',
            { verticalPosition: 'top', duration: 3000 }
          );
        }
      );
    }

    goBack(){
      this.router.navigate(['/admin/survey-history']);
    }

    formatDate(date : string){
      let aar = date.split('T');
      let ar = aar[0].split('-');
      return ar[2] + "-" + ar[1] + "-" + ar[0];
    }
  
  
    
    ngOnDestroy() {
      if (this.subscription) this.subscription.unsubscribe();
    }
}
