import { Route } from '@angular/router';
import { SurveyHistoryComponent } from './survey-history.component';
import { SurveyHistoryViewComponent } from './survey-history-view/survey-history-view.component';


export const routes: Route[] = [
    {
        path     : '',
        component: SurveyHistoryComponent
    },
    { path: 'view/:slug',   component: SurveyHistoryViewComponent}
];
