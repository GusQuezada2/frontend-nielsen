import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SurveyHistoryViewComponent } from './survey-history-view.component';

describe('SurveyHistoryViewComponent', () => {
  let component: SurveyHistoryViewComponent;
  let fixture: ComponentFixture<SurveyHistoryViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SurveyHistoryViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SurveyHistoryViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
