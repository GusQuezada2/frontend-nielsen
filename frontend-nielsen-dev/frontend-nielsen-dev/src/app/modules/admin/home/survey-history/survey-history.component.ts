import { Component, OnDestroy, OnInit } from '@angular/core';
import { TreoSplashScreenService } from '@treo/services/splash-screen/splash-screen.service';
import { FormService } from '@modules/admin/org/form/form.service';
import { Subscription } from 'rxjs';
import { Form } from '@models/form';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TreoAnimations } from '@treo/animations';
import { Session } from '@models/session';
import { ConstantService } from '@services/constant.service';
import { Group } from '@models/group';
import { Customer } from '@models/customer';
import { CustomerService } from '@modules/admin/client/customer/customer.service';
import { GroupService } from '@modules/admin/org/group/group.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-survey-history',
  templateUrl: './survey-history.component.html',
  styleUrls: ['./survey-history.component.scss'],
  providers: [ FormService, CustomerService, GroupService ],
  animations: [ TreoAnimations ]
})
export class SurveyHistoryComponent implements OnInit, OnDestroy {
  private subscription: Subscription = new Subscription();
  forms: Form[] = null;
  profile: Session;
  loading: boolean = true;
  currentPage:number=0;
  pageSize:number= ConstantService.paginationDesktop;
  totalElements:number;
  customers: Customer[] = null;
  groups:Group[];

  constructor(private splash : TreoSplashScreenService, private customerSrv : CustomerService, 
    private srv : FormService, private snack : MatSnackBar, private router : Router,
    private groupSrv : GroupService) { 
      this.splash.hide();
    }

  ngOnInit(): void {
    this.profile = JSON.parse(localStorage.getItem("profile"));
    this.getGroups();
  }

  private getGroups(){
    this.subscription.add(this.groupSrv.findAllActive().subscribe(
      response => {
        this.groups = response;
        this.getCustomers()
      }, error => {
        this.loading = false;

        this.snack.open('Se ha producido un error al intentar conseguir los grupos.', 'X',
            { verticalPosition: 'top', duration: 5000 }
        );
      }
    ));
  }

  private getCustomers(){
    this.subscription.add(this.customerSrv.findAllActive().subscribe(
      response => {
        this.customers = response;
        this.count();
      }, error => {
        this.loading = false;
        this.snack.open('Se ha producido un error al intentar conseguir los clientes.', 'X',
            { verticalPosition: 'top', duration: 5000 }
        );
      }
    ));
  }

  private count() {
    this.subscription.add(this.srv.findHistoryCount(this.profile.rut)
      .subscribe(
        response => {
          this.totalElements = response.count;
          this.find();
        },
        error => {
          this.loading = false;
          this.snack.open('Se ha producido un error al intentar conseguir la encuesta.', 'X',
            { verticalPosition: 'top', duration: 5000 }
          );
        }
      )
    );
  }

  private find() {
    this.subscription.add(this.srv.findHistoryByUser(this.profile.rut, this.currentPage)
      .subscribe(
        response => {
          this.forms = response;
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.snack.open('Se ha producido un error al intentar conseguir la encuesta.', 'X',
            { verticalPosition: 'top', duration: 5000 }
          );
        }
      )
    );
  }

  answer(slug : string){
    this.router.navigate(['/admin/my-surveys/answer/' + slug]);
  }

  getCustomer(slug : string){
    let customer = this.customers.filter( cus => {
      return cus.slug === slug;
    });
    return customer[0].title;
  }

  getGroup(slug : string){
    let group = this.groups.filter( grp => {
      return grp.slug === slug;
    });
    return group[0].title;
  }

  formatDate(date : string){
    let aar = date.split('T');
    let ar = aar[0].split('-');
    return ar[2] + "-" + ar[1] + "-" + ar[0];
  }


  onPageFired(event : any){
    this.currentPage = event.pageIndex;
    this.pageSize = event.pageSize;
    this.find();
  }

  ngOnDestroy() {
    if (this.subscription) this.subscription.unsubscribe();
  }
}