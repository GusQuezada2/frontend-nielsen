import { Route } from '@angular/router';
import { DashboardComponent } from '@modules/admin/home/dashboard/dashboard.component';

export const routes: Route[] = [
    {
        path     : '',
        component: DashboardComponent
    }
];
