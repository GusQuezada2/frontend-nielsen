import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MatDividerModule } from '@angular/material/divider';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { SharedModule } from '@shared/shared.module';
import { TreoCardModule } from '@treo/components/card';
import { TreoMessageModule } from '@treo/components/message';
import { DashboardComponent } from '@modules/admin/home/dashboard/dashboard.component';
import { routes } from '@modules/admin/home/dashboard/dashboard.routing';

@NgModule({
    declarations: [
        DashboardComponent
    ],
    imports     : [
        CommonModule,
        RouterModule.forChild(routes),
        MatDividerModule,
        MatProgressBarModule,
        TreoCardModule,
        TreoMessageModule,
        SharedModule
    ]
})
export class DashboardModule
{
}
