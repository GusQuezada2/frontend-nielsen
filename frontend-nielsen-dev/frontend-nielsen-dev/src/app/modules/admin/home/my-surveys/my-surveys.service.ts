import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AbstractHttpService } from '@services/abstract-http.service';
import { Observable } from 'rxjs';
import { File64 } from '@models/file';
import { map } from 'rxjs/operators';
import { MySurvey } from '@models/my-survey';

@Injectable()
export class MySurveysService extends AbstractHttpService {

  constructor(protected http: HttpClient) {
    super(http);
  } 

  count() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http
      .get<any>(
        this.apiUrl + '/my-surveys/count', httpOptions)
      .pipe(
        map(response => {
          return response;
        })
      );
  }

  findByUser(user: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.get<any>(`${this.apiUrl}/my-surveys/${user}`, httpOptions).pipe(
      map(response => {
        return response;
      })
    );
  }

  findByForm(form: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    let filter = '{ "where": { "form" : "' +  form + '" }}';
    return this.http.get<any>(`${this.apiUrl}/my-surveys/?filter=` + encodeURIComponent(filter), httpOptions).pipe(
      map(response => {
        return response;
      })
    );
  }
  add(survey: MySurvey) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'      
      })
    };
    return this.http.post<any>(`${this.apiUrl}/my-surveys`, survey, httpOptions).pipe(
      map(response => {
        return response;
      })
    );
  }

  update(slug: string, survey: MySurvey) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http
      .put<any>(`${this.apiUrl}/my-surveys/${slug}`, survey, httpOptions)
      .pipe(
        map(response => {
          return response;
        })
      );
  }

  send(slug: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http
      .put<any>(`${this.apiUrl}/my-surveys/send/${slug}`, {}, httpOptions)
      .pipe(
        map(response => {
          return response;
        })
      );
  }

  getPdfB64(slug: string): Observable<File64> {
    return this.http.get<File64>(`${this.apiUrl}/my-surveys/pdf/${slug}`);
  }
}
