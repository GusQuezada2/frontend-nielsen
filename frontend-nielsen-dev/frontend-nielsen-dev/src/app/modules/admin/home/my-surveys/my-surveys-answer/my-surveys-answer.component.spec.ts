import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MySurveysAnswerComponent } from './my-surveys-answer.component';

describe('MySurveysAnswerComponent', () => {
  let component: MySurveysAnswerComponent;
  let fixture: ComponentFixture<MySurveysAnswerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MySurveysAnswerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MySurveysAnswerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
