import { Route } from '@angular/router';
import { MySurveysComponent } from './my-surveys.component';
import { MySurveysAnswerComponent } from './my-surveys-answer/my-surveys-answer.component';

export const routes: Route[] = [
    {
        path     : '',
        component: MySurveysComponent
    },
    { path: 'view/:slug',   component: MySurveysAnswerComponent}
];
