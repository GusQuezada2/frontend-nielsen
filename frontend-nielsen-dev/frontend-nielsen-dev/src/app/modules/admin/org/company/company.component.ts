import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TreoSplashScreenService } from '@treo/services/splash-screen/splash-screen.service';
import { TreoAnimations } from '@treo/animations';
import { CompanyService } from './company.service';
import { Subscription} from 'rxjs';
import { Organization } from '@models/organization';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss'],
  providers: [ CompanyService ],
  animations: [ TreoAnimations]
})
export class CompanyComponent implements OnInit, OnDestroy {
  private subscription :Subscription = new Subscription();
  loading: boolean = true;
  org : Organization;
  orgForm: FormGroup;

  constructor(private splash : TreoSplashScreenService, private srv : CompanyService, 
    public formBuilder: FormBuilder, private snack : MatSnackBar) {
    this.splash.hide();
   }

  ngOnInit(): void {
    this.getData();
  }

  private getData(){
    this.subscription.add(this.srv.find()
      .subscribe(
        response => {
          this.orgForm =  this.formBuilder.group({
            name: this.formBuilder.control({value: response[0].title, disabled: true}),
            website: this.formBuilder.control({value: response[0].website, disabled: true}),
            address: this.formBuilder.control({value: response[0].address, disabled: true}),
            phone: this.formBuilder.control({value: response[0].phone, disabled: true}),
            email: this.formBuilder.control({value: response[0].email, disabled: true}),
            description: this.formBuilder.control({value: response[0].description, disabled: true}),
          });
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.snack.open('Se ha producido un error al intentar conseguir la compañia.', 'X',
            { verticalPosition: 'top', duration: 5000 }
          );
        }
      )
    );
  }
  ngOnDestroy() {
    if (this.subscription) this.subscription.unsubscribe();
  }
}
