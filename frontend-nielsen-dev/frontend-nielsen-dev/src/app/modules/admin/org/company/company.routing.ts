import { Route } from '@angular/router';
import { CompanyComponent } from './company.component';


export const routes: Route[] = [
    {
        path     : '',
        component: CompanyComponent
    }
];
