import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AbstractHttpService } from '@services/abstract-http.service';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Organization } from '@models/organization';
import { ConstantService } from '@services/constant.service';
@Injectable()
export class CompanyService extends AbstractHttpService {

  constructor(protected http: HttpClient) {
    super(http);
  } 

  remove(id: string, token: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: token
      })
    };
    return this.http.delete<any>(`${this.apiUrl}/organizations/${id}`, httpOptions).pipe(
      map(response => {
        return response;
      })
    );
  }

  find() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http
      .get<any>(
        this.apiUrl + '/organizations',
        httpOptions
      )
      .pipe(
        map(response => {
          return response;
        })
      );
  }

  count() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http
      .get<any>(
        this.apiUrl + '/organizations/count', httpOptions)
      .pipe(
        map(response => {
          return response;
        })
      );
  }

  findByName(name: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    const filter =
      '{ "where" : { "title": "' + name + '", "linkDerivation": ""}}';
    return this.http
      .get<any>(
        this.apiUrl + '/organizations?filter=' + encodeURIComponent(filter),
        httpOptions
      )
      .pipe(
        map(response => {
          return response;
        })
      );
  }

  findById(id: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.get<any>(`${this.apiUrl}/organizations/${id}`, httpOptions).pipe(
      map(response => {
        return response;
      })
    );
  }
  add(org: Organization) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post<any>(`${this.apiUrl}/organizations`, org, httpOptions).pipe(
      map(response => {
        return response;
      })
    );
  }

  update(id: string, org: Organization, token: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: token
      })
    };
    return this.http
      .patch<any>(`${this.apiUrl}/organizations/${id}`, org, httpOptions)
      .pipe(
        map(response => {
          return response;
        })
      );
  }

  uploadImage(file: any) {
    const httpOptions = {
      headers: new HttpHeaders({
        Content: 'multipart/form-data'
      })
    };

    const formData: FormData = new FormData();
    formData.append('file', file, file.name);

    return this.http
      .post<any>(`${this.apiUrl}/fileupload`, formData, httpOptions)
      .pipe(
        map(response => {
          return response;
        })
      );
  }
}
