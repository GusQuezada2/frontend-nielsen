import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription} from 'rxjs';
import { UtilService } from '@services/util.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Department } from '@models/department';
import { Organization } from '@models/organization';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TreoSplashScreenService } from '@treo/services/splash-screen/splash-screen.service';
import { TreoAnimations } from '@treo/animations';
import { DepartmentService } from './../department.service';
import { CompanyService } from '@modules/admin/org/company/company.service';

@Component({
  selector: 'app-department-view',
  templateUrl: './department-view.component.html',
  styleUrls: ['./department-view.component.scss'],
  providers: [ DepartmentService, CompanyService ],
  animations: TreoAnimations
})
export class DepartmentViewComponent implements OnInit, OnDestroy {
  private subscription :Subscription = new Subscription();
  slug : string;
  screenType: string;
  departmentForm: FormGroup;
  privilegeForm: FormGroup;
  loading : boolean = true;
  deparment : Department;
  companies:Organization[];
  constructor(private utilSrv : UtilService, private departmentSrv : DepartmentService,
    private router : Router, private activatedRoute: ActivatedRoute,  private companySrv : CompanyService,
    private splash : TreoSplashScreenService, private snack : MatSnackBar, public formBuilder: FormBuilder) {
    this.splash.hide();
  }

  ngOnInit() {
    this.subscription.add(this.activatedRoute.params.subscribe(params => { 
      this.slug = params['slug'];
      this.getDepartment();
    }));

    this.subscription.add(this.utilSrv.screenType$.subscribe(
      screen => { 
        this.screenType = screen;
      }
    ));
  }

  private getDepartment(){
    this.subscription.add(this.departmentSrv.findById(this.slug).subscribe(
      response => {
        this.deparment = response;
        this.departmentForm = this.formBuilder.group({
          slug: this.formBuilder.control({value: response.slug, disabled: true}),
          title: this.formBuilder.control({value: response.title, disabled: true}),
          description: this.formBuilder.control({value: response.description, disabled: true}),
          status: this.formBuilder.control({value: response.status.toString(), disabled: true}),
          company:  this.formBuilder.control({value: '', disabled: true})
        });
        this.getCompany();
      }, error => {
        this.loading = false;
        this.snack.open('Se ha producido un error al intentar conseguir el departamento.', 'X',
            { verticalPosition: 'top', duration: 5000 }
        );
      }
    ));
  }
  private getCompany(){
    this.subscription.add(this.companySrv.find().subscribe(
      response => {
        this.companies = response;
        this.departmentForm.controls.company.setValue(response[0]);
        this.loading = false;
      }, error => {
        this.loading = false;
        this.snack.open('Se ha producido un error al intentar conseguir la compañia.', 'X',
            { verticalPosition: 'top', duration: 5000 }
        );
      }
    ));
  }

  goBack(){
    this.router.navigate(['/admin/org/department']);
  }

  ngOnDestroy() {
    if (this.subscription) this.subscription.unsubscribe();
  }

}
