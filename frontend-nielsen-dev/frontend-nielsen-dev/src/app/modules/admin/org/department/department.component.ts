import { Component, OnDestroy, OnInit } from '@angular/core';
import { TreoSplashScreenService } from '@treo/services/splash-screen/splash-screen.service';
import { DepartmentService } from './department.service';
import { Subscription } from 'rxjs';
import { Department } from '@models/department';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TreoAnimations } from '@treo/animations';

@Component({
  selector: 'app-department',
  templateUrl: './department.component.html',
  styleUrls: ['./department.component.scss'],
  providers: [ DepartmentService ],
  animations: [ TreoAnimations ]

})
export class DepartmentComponent implements OnInit, OnDestroy {
  private subscription: Subscription = new Subscription();
  departments: Department[] = null;
  loading: boolean = true;
  constructor(private splash : TreoSplashScreenService, 
    private srv : DepartmentService, private snack : MatSnackBar) { 
      this.splash.hide();
    }

  ngOnInit(): void {
    this.find();
  }

  private find() {
    this.subscription.add(this.srv.find()
      .subscribe(
        response => {
          this.departments = response;
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.snack.open('Se ha producido un error al intentar conseguir los departamentos.', 'X',
            { verticalPosition: 'top', duration: 5000 }
          );
        }
      )
    );
  }
  ngOnDestroy() {
    if (this.subscription) this.subscription.unsubscribe();
  }
}