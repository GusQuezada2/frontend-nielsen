import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AbstractHttpService } from '@services/abstract-http.service';
import { map } from 'rxjs/operators';

@Injectable()
export class DepartmentService extends AbstractHttpService {

  constructor(protected http: HttpClient) {
    super(http);
  } 

  find() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    let filter = '{ "order": [ "title ASC"]}';
    return this.http
      .get<any>(
        this.apiUrl + '/departments?filter=' + encodeURIComponent(filter),
        httpOptions
      )
      .pipe(
        map(response => {
          return response;
        })
      );
  }

  findById(id: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.get<any>(`${this.apiUrl}/departments/${id}`, httpOptions).pipe(
      map(response => {
        return response;
      })
    );
  }

  findAllActive() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    let filter = '{ "where": { "status" : 1 }, "order": [ "title ASC"]}';
    return this.http
      .get<any>(
        this.apiUrl + '/departments?filter=' + encodeURIComponent(filter),
        httpOptions
      )
      .pipe(
        map(response => {
          return response;
        })
      );
  }
}
