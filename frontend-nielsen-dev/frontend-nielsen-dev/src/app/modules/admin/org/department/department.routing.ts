import { Route } from '@angular/router';
import { DepartmentComponent } from './department.component';
import { DepartmentViewComponent } from './department-view/department-view.component';


export const routes: Route[] = [
    {
        path     : '',
        component: DepartmentComponent
    },
    { path: 'view/:slug',   component: DepartmentViewComponent},
];
