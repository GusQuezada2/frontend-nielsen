import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription} from 'rxjs';
import { UtilService } from '@services/util.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Customer } from '@models/customer';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TreoSplashScreenService } from '@treo/services/splash-screen/splash-screen.service';
import { TreoAnimations } from '@treo/animations';
import { ChargeService } from '../charge.service';

@Component({
  selector: 'app-charge-view',
  templateUrl: './charge-view.component.html',
  styleUrls: ['./charge-view.component.scss'],
  providers: [ ChargeService ],
  animations: TreoAnimations
})
export class ChargeViewComponent implements OnInit, OnDestroy {
  private subscription :Subscription = new Subscription();
  slug : string;
  screenType: string;
  chargeForm: FormGroup;
  loading : boolean = true;
  customer: Customer;
  constructor(private utilSrv : UtilService, private srv : ChargeService,
    private router : Router, private activatedRoute: ActivatedRoute,
    private splash : TreoSplashScreenService, private snack : MatSnackBar, public formBuilder: FormBuilder) {
    this.splash.hide();
  }

  ngOnInit() {
    this.subscription.add(this.activatedRoute.params.subscribe(params => { 
      this.slug = params['slug'];
      this.getCharge();
    }));

    this.subscription.add(this.utilSrv.screenType$.subscribe(
      screen => { 
        this.screenType = screen;
      }
    ));
  }

  private getCharge(){
    this.subscription.add(this.srv.findById(this.slug).subscribe(
      response => {
        this.customer = response;
        this.chargeForm = this.formBuilder.group({
          slug: this.formBuilder.control({value: response.slug, disabled: true}),
          title: this.formBuilder.control({value: response.title, disabled: true}),
          description: this.formBuilder.control({value: response.description, disabled: true}),
          status: this.formBuilder.control({value: response.status.toString(), disabled: true}),
        });
        this.loading = false;
      }, error => {
        this.loading = false;
        this.snack.open('Se ha producido un error al intentar conseguir el cargo.', 'X',
            { verticalPosition: 'top', duration: 5000 }
        );
      }
    ));
  }

  goBack(){
    this.router.navigate(['/admin/org/charge']);
  }

  ngOnDestroy() {
    if (this.subscription) this.subscription.unsubscribe();
  }

}
