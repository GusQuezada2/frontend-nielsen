import { Component, OnDestroy, OnInit } from '@angular/core';
import { TreoSplashScreenService } from '@treo/services/splash-screen/splash-screen.service';
import { ChargeService } from './charge.service';
import { Subscription } from 'rxjs';
import { Customer } from '@models/customer';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TreoAnimations } from '@treo/animations';

@Component({
  selector: 'app-charge',
  templateUrl: './charge.component.html',
  styleUrls: ['./charge.component.scss'],
  providers: [ ChargeService ],
  animations: [ TreoAnimations ]
})
export class ChargeComponent implements OnInit, OnDestroy {
  private subscription: Subscription = new Subscription();
  charges: Customer[] = null;
  loading: boolean = true;
  constructor(private splash : TreoSplashScreenService, 
    private srv : ChargeService, private snack : MatSnackBar) { 
      this.splash.hide();
    }

  ngOnInit(): void {
    this.find();
  }

  private find() {
    this.subscription.add(this.srv.find()
      .subscribe(
        response => {
          this.charges = response;
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.snack.open('Se ha producido un error al intentar conseguir los cargos.', 'X',
            { verticalPosition: 'top', duration: 5000 }
          );
        }
      )
    );
  }
  ngOnDestroy() {
    if (this.subscription) this.subscription.unsubscribe();
  }
}