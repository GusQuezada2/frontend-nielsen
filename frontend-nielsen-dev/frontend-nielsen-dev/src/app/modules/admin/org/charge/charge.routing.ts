import { Route } from '@angular/router';
import { ChargeComponent } from './charge.component';
import { ChargeViewComponent } from './charge-view/charge-view.component';


export const routes: Route[] = [
    {
        path     : '',
        component: ChargeComponent
    },
    { path: 'view/:slug',   component: ChargeViewComponent},
];
