import { Route } from '@angular/router';
import { FormComponent } from './form.component';
import { FormEditComponent } from './form-edit/form-edit.component';
import { FormViewComponent } from './form-view/form-view.component';
import { FormAddComponent } from './form-add/form-add.component';
import { FormStatusComponent } from './form-status/form-status.component';

export const routes: Route[] = [
    {
        path     : '',
        component: FormComponent
    },
    { path: 'add',          component: FormAddComponent},
    { path: 'view/:slug',   component: FormViewComponent},
    { path: 'edit/:slug',   component: FormEditComponent},
    { path: 'status/:slug',   component: FormStatusComponent}
];

