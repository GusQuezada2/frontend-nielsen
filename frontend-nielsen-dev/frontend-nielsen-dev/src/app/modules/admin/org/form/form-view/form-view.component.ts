import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription} from 'rxjs';
import { UtilService } from '@services/util.service';
import { I18nService } from '@services/i18n.service';
import { FormBuilder, FormGroup, Validators , FormControl} from '@angular/forms';
import { FormService } from '../form.service';
import { Form } from '@models/form';
import { Role } from '@models/role';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TreoSplashScreenService } from '@treo/services/splash-screen/splash-screen.service';
import { TreoAnimations } from '@treo/animations';
import { Group } from '@models/group';
import { Customer } from '@models/customer';
import { CustomerService } from '@modules/admin/client/customer/customer.service';
import { MatStepper } from '@angular/material/stepper';
import { GroupService } from '@modules/admin/org/group/group.service';
@Component({
  selector: 'app-form-view',
  templateUrl: './form-view.component.html',
  styleUrls: ['./form-view.component.scss'],
  providers: [ FormService, GroupService, CustomerService ],
  animations: TreoAnimations
})
export class FormViewComponent implements OnInit, OnDestroy {
  private subscription :Subscription = new Subscription();
  screenType: string;
  customers: Customer[] = null;
  roles: Role[] = null;
  step1Form: FormGroup;
  step2Form: FormGroup;
  statusForm: FormGroup;
  form: Form = null;
  formAdd: Form = null;
  loading : boolean = true;
  loadingSrv : boolean = false;
  groups:Group[];
  relation:boolean = false;
  code: string;
  questionsList = [];

    constructor(private activatedRoute: ActivatedRoute, private utilSrv : UtilService, private srv : FormService,
      private router : Router, public i18n : I18nService, private customerSrv : CustomerService,
      private splash : TreoSplashScreenService, private snack : MatSnackBar, public formBuilder: FormBuilder,
      private groupSrv : GroupService) {
      this.splash.hide();
     }
  
    ngOnInit() {
      this.subscription.add(this.utilSrv.screenType$.subscribe(
        screen => { 
          this.screenType = screen;
        }
      ));
      this.subscription.add(this.activatedRoute.params.subscribe(params => { 
        this.code = params['slug'];
        this.getGroups();
      }));
    }
  
    private getGroups(){
      this.subscription.add(this.groupSrv.findAllActive().subscribe(
        response => {
          this.groups = response;
          this.getCustomers()
        }, error => {
          this.loading = false;

          this.snack.open('Se ha producido un error al intentar conseguir los grupos.', 'X',
              { verticalPosition: 'top', duration: 5000 }
          );
        }
      ));
    }
    private getCustomers(){
      this.subscription.add(this.customerSrv.findAllActive().subscribe(
        response => {
          this.customers = response;
          this.getForm();
        }, error => {
          this.loading = false;
          this.snack.open('Se ha producido un error al intentar conseguir los clientes.', 'X',
              { verticalPosition: 'top', duration: 5000 }
          );
        }
      ));
    }
  
    getForm(){
      this.subscription.add(this.srv.findById(this.code).subscribe(
        response => {
          this.form = response;
          this.step1Form = this.formBuilder.group({
            title : [ { value : response.title, disabled: true}],
            description : [{ value : response.description, disabled: true}],
            customer : [ { value : response.customer, disabled: true}],
            mode:[ { value : response.group ? '1' : '2', disabled: true}],
            group : [ { value : response.group ? response.group : null, disabled: true}],
            ot : [ { value : response.ot ? response.ot : null , disabled: true}],
            createdAt:[ {value: response.createdAt, disabled: true}],
            createdBy: [{value: response.createdBy, disabled: true}],
            slug: [{value: response.slug, disabled: true}]
          });
          
          this.statusForm = this.formBuilder.group({
            status: this.formBuilder.control({value: response.status.toString(), disabled: true}),
            vigency: [{value: response.vigencyAt === null || response.vigencyAt.substring(0, 10) === '5000-01-01' ? '1' : '2', disabled: true}],
            vigencyDate: [{value: response.vigencyAt === null || response.vigencyAt.substring(0, 10) === '5000-01-01' ? '' : response.vigencyAt, disabled: true}],
            suspendDate: [{value: response.suspendAt, disabled: true}],
            deleteAt: [{value: response.deleteAt, disabled: true}],
            publishAt: [{value: response.publishAt, disabled: true}],
            createdAt:[ {value: response.createdAt, disabled: true}]
          });
          this.step2Form = this.formBuilder.group({});
          if (this.form.questions){
            for (let i = 0; i < this.form.questions.length; i++){
              this.questionsList.push({ alternatives : []})
              this.step2Form.addControl('question' + (this.questionsList.length - 1),
                new FormControl({ value: this.form.questions[i].title, disabled: true})
              );
    
              for (let j = 0; j < this.form.questions[i].alternatives.length; j++){
                let name = 'alternative_' + i + '_' + (this.questionsList[i].alternatives.length);
                this.questionsList[i].alternatives.push(name);
                this.step2Form.addControl(name,
                  new FormControl({ value: this.form.questions[i].alternatives[j], disabled: true})
                );
              }
            }
          }
          this.loading = false;
        }, error => {
          this.loading = false;
          this.snack.open('Se ha producido un error al intentar conseguir el formulario.', 'X',
              { verticalPosition: 'top', duration: 5000 }
          );
        }
      ));
    }   
  
    goBack(){
      this.router.navigate(['/admin/org/form']);
    }
  
    goPrevious(stepper: MatStepper){
      stepper.previous();
  }
  
    goForward2(stepper: MatStepper){
      stepper.next();
    }
    goForward3(stepper: MatStepper){
      stepper.next();
    }
  
    ngOnDestroy() {
      if (this.subscription) this.subscription.unsubscribe();
    }
}
