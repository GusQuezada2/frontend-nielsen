import { Component, OnDestroy, OnInit } from '@angular/core';
import { TreoSplashScreenService } from '@treo/services/splash-screen/splash-screen.service';
import { FormService } from './form.service';
import { Subscription } from 'rxjs';
import { Form } from '@models/form';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TreoAnimations } from '@treo/animations';
import { Router } from '@angular/router';
import { ConstantService } from '@services/constant.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
  providers: [ FormService ],
  animations: [ TreoAnimations ]

})
export class FormComponent implements OnInit, OnDestroy {
  private subscription: Subscription = new Subscription();
  forms: Form[] = null;
  loading: boolean = true;
  currentPage:number=0;
  pageSize:number= ConstantService.paginationDesktop;
  totalElements:number;
  constructor(private splash : TreoSplashScreenService, private router : Router, 
    private srv : FormService, private snack : MatSnackBar) { 
      this.splash.hide();
    }

  ngOnInit(): void {
    this.getCount();
  }

  private getCount(){
    this.subscription.add(this.srv.count()
      .subscribe(
        response => {
          this.totalElements = response.count;
          this.find();
        },
        error => {
          this.loading = false;
          this.snack.open('Se ha producido un error al intentar conseguir los formularios.', 'X',
            { verticalPosition: 'top', duration: 5000 }
          );
        }
      )
    );
  }

  private find() {
    this.subscription.add(this.srv.findAll()
      .subscribe(
        response => {
          this.forms = response;
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.snack.open('Se ha producido un error al intentar conseguir los formularios.', 'X',
            { verticalPosition: 'top', duration: 5000 }
          );
        }
      )
    );
  }

  edit(slug : string){
    this.router.navigate(['/admin/org/form/edit/' + slug ]);
  }

  changeStatus(slug : string){
    this.router.navigate(['/admin/org/form/status/' + slug ]);
  }


  remove(rut : string){
    this.loading = true;
    this.subscription.add(this.srv.remove(rut)
      .subscribe(
        response => {
          this.find();
          this.snack.open('Se ha eliminado satisfactoriamente el formulario.', 'X',
            { verticalPosition: 'top', duration: 5000 }
          );
        },
        err => {
          this.loading = false;
          this.snack.open(err.error.error.message, 'X',
            { verticalPosition: 'top', duration: 5000 }
          );
        }
      )
    );
  }

  onPageFired(event : any){
    this.currentPage = event.pageIndex;
    this.pageSize = event.pageSize;
    this.find();
  }

  ngOnDestroy() {
    if (this.subscription) this.subscription.unsubscribe();
  }

  add(){
    this.router.navigate(['/admin/org/form/add']);
  }
}