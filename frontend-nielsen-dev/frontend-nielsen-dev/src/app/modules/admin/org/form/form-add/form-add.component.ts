import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription} from 'rxjs';
import { UtilService } from '@services/util.service';
import { I18nService } from '@services/i18n.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { FormService } from '../form.service';
import { Form } from '@models/form';
import { Role } from '@models/role';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TreoSplashScreenService } from '@treo/services/splash-screen/splash-screen.service';
import { TreoAnimations } from '@treo/animations';
import { Group } from '@models/group';
import { Customer } from '@models/customer';
import { CustomerService } from '@modules/admin/client/customer/customer.service';
import { justLetterValidatorLastAndFirstName,
  selectAnOptionValidator } from '@shared/utils/form-validators';
import { MatStepper } from '@angular/material/stepper';
import { GroupService } from '@modules/admin/org/group/group.service';

@Component({
  selector: 'app-form-add',
  templateUrl: './form-add.component.html',
  styleUrls: ['./form-add.component.scss'],
  providers: [ FormService, GroupService, CustomerService ],
  animations: TreoAnimations
})
export class FormAddComponent implements OnInit, OnDestroy {
  private subscription :Subscription = new Subscription();
  screenType: string;
  customers: Customer[] = null;
  roles: Role[] = null;
  step1Form: FormGroup;
  step2Form: FormGroup;
  form: Form = null;
  formAdd: Form = null;
  loading : boolean = true;
  loadingSrv : boolean = false;
  groups:Group[];
  relation:boolean = false;
  questionsList = [];
  @ViewChild('stepper') private myStepper: MatStepper;

  constructor(private utilSrv : UtilService, private srv : FormService,
    private router : Router, public i18n : I18nService, private customerSrv : CustomerService,
    private splash : TreoSplashScreenService, private snack : MatSnackBar, public formBuilder: FormBuilder,
    private groupSrv : GroupService) {
    this.initForms();
    this.splash.hide();
   }

  ngOnInit() {
    this.subscription.add(this.utilSrv.screenType$.subscribe(
      screen => { 
        this.screenType = screen;
      }
    ));

    this.getGroups();
  }

  private getGroups(){
    this.subscription.add(this.groupSrv.findAllActive().subscribe(
      response => {
        this.groups = response;
        this.getCustomers()
      }, error => {
        this.loading = false;
        this.snack.open('Se ha producido un error al intentar conseguir los grupos.', 'X',
            { verticalPosition: 'top', duration: 5000 }
        );
      }
    ));
  }
  private getCustomers(){
    this.subscription.add(this.customerSrv.findAllActive().subscribe(
      response => {
        this.customers = response;
        this.loading = false;
      }, error => {
        this.loading = false;
        this.snack.open('Se ha producido un error al intentar conseguir los clientes.', 'X',
            { verticalPosition: 'top', duration: 5000 }
        );
      }
    ));
  }

  initForms(){

    this.step1Form = this.formBuilder.group({
      title : ['', [ Validators.required, Validators.minLength(3),Validators.maxLength(500)]],
      description : ['', [ Validators.required, Validators.minLength(3),Validators.maxLength(5000)]],
      customer : [ null],
      mode:[ null, [ Validators.required, selectAnOptionValidator]],
      group : [ null, ],
      ot : [ null]
    });

    this.step2Form = this.formBuilder.group({});
  }


  
  private save(){
    this.subscription.add(this.srv.add(this.createForm()).subscribe(
      response => {
        this.formAdd = response;
        this.snack.open('Se ha agregado satisfactoriamente el formulario.', 'X',
            { panelClass: ['success'], verticalPosition: 'top', duration: 5000 }
        );
        this.loadingSrv = false;
        this.goForward2(this.myStepper);
      }, error => {
        this.loadingSrv = false;
        this.snack.open('Se ha producido un error al intentar guardar el formulario.', 'X',
            { verticalPosition: 'top', duration: 5000 }
        );
      }
    ));
  }

  add(){
    this.loadingSrv = true;
    this.subscription.add(this.srv.findByName(this.step1Form.controls.title.value).subscribe(
      response => {
        if (response.length === 0){
          this.save();
        } else {
          this.loadingSrv = false;
          this.snack.open('Ya existe un formulario con ese nombre', 'X',
          { verticalPosition: 'top', duration: 5000 });
        }
      }, error => {
        this.loadingSrv = false;
        this.snack.open('Ya existe un formulario con ese nombre', 'X',
            { verticalPosition: 'top', duration: 5000 });
      }
    ));
  }

  addQuestion() {
    this.questionsList.push({ alternatives : []})
    this.step2Form.addControl(
      'question' + (this.questionsList.length - 1),
      new FormControl(
        '',
        Validators.compose([Validators.required, Validators.minLength(3)])
      )
    );
  }

  addAlternative(question: number) {
    let name = 'alternative_' + question + '_' + (this.questionsList[question].alternatives.length);
    this.questionsList[question].alternatives.push(name);
    this.step2Form.addControl(
      name,
      new FormControl(
        '',
        Validators.compose([Validators.required, Validators.minLength(1)])
      )
    );
  }

  removeQuestion(index : number) {
    const control = 'question' + index;
    for (let i = 0; i < this.questionsList[index].alternatives.length; i++){
      delete this.step2Form.controls[this.questionsList[index].alternatives[i]];
    }
    this.questionsList.splice(index, 1);
    delete this.step2Form.controls[control];
  }

  removeAlternative(index : number, answer : number) {
    const control = 'alternative_' + index  + '_' + answer;
    this.questionsList[index].alternatives.splice(answer, 1);
    delete this.step2Form.controls[control];
  }

  private createForm(){
    let group = "";
    let ot = "";
    if (this.step1Form.controls.mode.value === '1'){
      group = this.step1Form.controls.group.value;
      ot = "";
    }
    if (this.step1Form.controls.mode.value === '2'){
      ot = this.step1Form.controls.ot.value;
      group = "";
    }
    let user: any = {
      customer: this.step1Form.controls.customer.value !== null ? this.step1Form.controls.customer.value : '',
      title: this.step1Form.controls.title.value,
      description: this.step1Form.controls.description.value,
      ot: ot,
      group: group,
      questions: []

    }
    return user;
  }

  saveQuestions(){
    this.loadingSrv = true;
    this.subscription.add(this.srv.questions(this.formAdd.slug, this.createQuestions()).subscribe(
      response => {
          this.loadingSrv = false;
          this.snack.open('Se ha guardado satisfactoriamente la encuesta.', 'X',
          { panelClass: ['success'], verticalPosition: 'top', duration: 5000 });
          this.goBack();
      }, error => {
        this.loadingSrv = false;
        this.snack.open('Se ha producido un error al intentar guardar la encuesta', 'X',
            { verticalPosition: 'top', duration: 5000 });
      }
    ));
  }

  createQuestions(){
    let questions: any = []
    for (let i = 0; i < this.questionsList.length; i++){
      questions.push({ title: this.step2Form.controls['question' + i ].value, alternatives : []})
      for (let j = 0; j < this.questionsList[i].alternatives.length; j++ ){
        questions[i].alternatives.push(this.step2Form.controls['alternative_' + i + '_' + j].value);
      }
    }
    return questions
  }

  goBack(){
    this.router.navigate(['/admin/org/form']);
  }

  goPrevious(stepper: MatStepper){
    stepper.previous();
}

  goForward2(stepper: MatStepper){
    stepper.next();
  }
  goForward3(stepper: MatStepper){
    stepper.next();
  }

  ngOnDestroy() {
    if (this.subscription) this.subscription.unsubscribe();
  }

}