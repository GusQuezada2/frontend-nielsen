import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GroupComponent } from './group.component';
import { RouterModule } from '@angular/router';
import { routes } from './group.routing';
import { SharedModule } from 'app/shared/shared.module';
import { TreoCardModule } from '@treo/components/card';
import { TreoMessageModule } from '@treo/components/message';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatPaginatorModule} from '@angular/material/paginator';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatMenuModule } from '@angular/material/menu';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatStepperModule } from '@angular/material/stepper';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { GroupViewComponent } from './group-view/group-view.component';
import { GroupEditComponent } from './group-edit/group-edit.component';
import { GroupAddComponent } from './group-add/group-add.component';


@NgModule({
  declarations: [GroupComponent, GroupViewComponent, GroupEditComponent, GroupAddComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    TreoCardModule,
    TreoMessageModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatButtonModule,
    MatCheckboxModule,
    MatDividerModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatRadioModule,
    MatSelectModule,
    MatStepperModule,
    MatMomentDateModule,
    MatDatepickerModule,
    MatTooltipModule 
  ]
})
export class GroupModule { }
