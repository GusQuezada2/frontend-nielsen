import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { UtilService } from '@services/util.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TreoSplashScreenService } from '@treo/services/splash-screen/splash-screen.service';
import { TreoAnimations } from '@treo/animations';
import { GroupService } from './../group.service';
import { checkedOptionValidator} from '@shared/utils/form-validators';
import { CompanyService } from '@modules/admin/org/company/company.service';
import { Organization } from '@models/organization';
import { Router, ActivatedRoute } from '@angular/router';
import { Group } from '@models/group';

@Component({
  selector: 'app-group-edit',
  templateUrl: './group-edit.component.html',
  styleUrls: ['./group-edit.component.scss'],
  providers: [ GroupService, CompanyService ],
  animations: TreoAnimations
})

export class GroupEditComponent implements OnInit, OnDestroy {
  private subscription :Subscription = new Subscription();
  screenType: string;
  groupForm: FormGroup;
  loading : boolean = true;
  companies:Organization[];
  slug : string;
  group : Group;

  constructor(private utilSrv : UtilService, private groupSrv : GroupService,
    private router : Router, private companySrv : CompanyService, private activatedRoute: ActivatedRoute, 
    private splash : TreoSplashScreenService, private snack : MatSnackBar, public formBuilder: FormBuilder) {
    this.splash.hide();
   }

  ngOnInit() {
    this.subscription.add(this.activatedRoute.params.subscribe(params => { 
      this.slug = params['slug'];
      this.getGroup();
    }));

    this.subscription.add(this.utilSrv.screenType$.subscribe(
      screen => { 
        this.screenType = screen;
      }
    ));
  }
  private getCompany(){
    this.subscription.add(this.companySrv.find().subscribe(
      response => {
        this.companies = response;
        this.groupForm.controls.company.setValue(response[0]);
        this.loading = false;
      }, error => {
        this.loading = false;
        this.snack.open('Se ha producido un error al intentar conseguir la compañia.', 'X',
            { verticalPosition: 'top', duration: 5000 }
        );
      }
    ));
  }

  private getGroup(){
    this.subscription.add(this.groupSrv.findById(this.slug)
      .subscribe(
        response => {
          this.group = response;
          this.groupForm = this.formBuilder.group({
            title: this.formBuilder.control({value: response.title, disabled: false},  [ Validators.required, Validators.minLength(3),Validators.maxLength(60)]),
            description: this.formBuilder.control({value: response.description, disabled: false}, [ Validators.required, Validators.minLength(3),Validators.maxLength(5000)]),
            status: this.formBuilder.control({value: response.status.toString(), disabled: false}, [ Validators.required, checkedOptionValidator]),
            company:  this.formBuilder.control({value: '', disabled: true})
          });
          this.getCompany();
        },
        error => {
          this.loading = false;
          this.snack.open('Se ha producido un error al intentar conseguir el grupo.', 'X',
            { verticalPosition: 'top', duration: 5000 }
          );
        }
      )
    );
  }


  private save(){
    this.subscription.add(this.groupSrv.update(this.slug, this.createGroup()).subscribe(
      response => {
        this.loading = false;
        this.snack.open('Se ha editado satisfactoriamente el grupo.', 'X',
            { panelClass: ['success'], verticalPosition: 'top', duration: 5000 }
        );
        this.goBack();
      }, error => {
        this.loading = false;
        this.snack.open('Se ha producido un error al intentar editar el grupo.', 'X',
            { verticalPosition: 'top', duration: 5000 }
        );
      }
    ));
  }

  add(){
    this.loading = true;
    this.subscription.add(this.groupSrv.findByName(this.groupForm.controls.title.value).subscribe(
      response => {
        if (response.length === 0 || (response.length === 1 && this.groupForm.controls.title.value === this.group.title)){
          this.save();
        } else {
          this.loading = false;
          this.snack.open('Ya existe un grupo con ese nombre', 'X',
          { verticalPosition: 'top', duration: 5000 });
        }
      }, error => {
        this.loading = false;
        this.snack.open('Ya existe un grupo con ese nombre', 'X',
            { verticalPosition: 'top', duration: 5000 });
      }
    ));
  }

  private createGroup(){
    let group = {
      status: Number.parseInt(this.groupForm.controls.status.value),
      title: this.groupForm.controls.title.value,
      description: this.groupForm.controls.description.value,
      organization: this.groupForm.controls.company.value.slug
    }
    return group;
  }

  goBack(){
    this.router.navigate(['/admin/org/group']);
  }
  ngOnDestroy() {
    if (this.subscription) this.subscription.unsubscribe();
  }
}