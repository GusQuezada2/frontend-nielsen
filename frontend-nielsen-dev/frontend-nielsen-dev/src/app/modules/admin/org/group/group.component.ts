import { Component, OnDestroy, OnInit } from '@angular/core';
import { TreoSplashScreenService } from '@treo/services/splash-screen/splash-screen.service';
import { GroupService } from './group.service';
import { Subscription } from 'rxjs';
import { Group } from '@models/group';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TreoAnimations } from '@treo/animations';
import { Router } from '@angular/router';
import { ConstantService } from '@services/constant.service';

@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.scss'],
  providers: [ GroupService ],
  animations: [ TreoAnimations ]
})
export class GroupComponent implements OnInit, OnDestroy {
  private subscription: Subscription = new Subscription();
  groups: Group[] = null;
  loading: boolean = true;
  currentPage:number=0;
  pageSize:number= ConstantService.paginationDesktop;
  totalElements:number;
  constructor(private splash : TreoSplashScreenService, private router : Router, 
    private srv : GroupService, private snack : MatSnackBar) { 
      this.splash.hide();
    }

  ngOnInit(): void {
    this.getCount();
  }

  private getCount(){
    this.subscription.add(this.srv.count()
      .subscribe(
        response => {
          this.totalElements = response.count;
          this.find();
        },
        error => {
          this.loading = false;
          this.snack.open('Se ha producido un error al intentar conseguir los grupos.', 'X',
            { verticalPosition: 'top', duration: 5000 }
          );
        }
      )
    );
  }

  private find() {
    this.subscription.add(this.srv.findAll()
      .subscribe(
        response => {
          this.groups = response;
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.snack.open('Se ha producido un error al intentar conseguir los grupos.', 'X',
            { verticalPosition: 'top', duration: 5000 }
          );
        }
      )
    );
  }
  edit(slug : string){
    this.router.navigate(['/admin/org/group/edit/' + slug ]);
  }

  remove(slug : string){
    this.loading = true;
    this.subscription.add(this.srv.remove(slug)
      .subscribe(
        response => {
          this.find();
          this.snack.open('Se ha eliminado satisfactoriamente el grupo.', 'X',
            { verticalPosition: 'top', duration: 5000 }
          );
        },
        err => {
          this.loading = false;
          this.snack.open(err.error.error.message, 'X',
            { verticalPosition: 'top', duration: 5000 }
          );
        }
      )
    );
  }

  onPageFired(event : any){
    this.currentPage = event.pageIndex;
    this.pageSize = event.pageSize;
    this.loading = true;
    this.find();
  }
  add(){
    this.router.navigate(['/admin/org/group/add']);
  }
  ngOnDestroy() {
    if (this.subscription) this.subscription.unsubscribe();
  }
}