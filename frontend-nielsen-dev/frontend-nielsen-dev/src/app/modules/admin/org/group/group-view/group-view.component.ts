import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription} from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { UtilService } from '@services/util.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TreoSplashScreenService } from '@treo/services/splash-screen/splash-screen.service';
import { TreoAnimations } from '@treo/animations';
import { GroupService } from './../group.service';
import { CompanyService } from '@modules/admin/org/company/company.service';
import { Organization } from '@models/organization'
@Component({
  selector: 'app-group-view',
  templateUrl: './group-view.component.html',
  styleUrls: ['./group-view.component.scss'],
  providers: [ GroupService, CompanyService ],
  animations: TreoAnimations
})
export class GroupViewComponent implements OnInit, OnDestroy {
  private subscription :Subscription = new Subscription();
  screenType: string;
  groupForm: FormGroup;
  loading : boolean = true;
  companies:Organization[];
  slug : string;
  constructor(private utilSrv : UtilService, private groupSrv : GroupService,
    private router : Router, private companySrv : CompanyService,  private activatedRoute: ActivatedRoute, 
    private splash : TreoSplashScreenService, private snack : MatSnackBar, public formBuilder: FormBuilder) {
    this.splash.hide();
   }

  ngOnInit() {
    this.subscription.add(this.activatedRoute.params.subscribe(params => { 
      this.slug = params['slug'];
      this.getGroup();
    }));

    this.subscription.add(this.utilSrv.screenType$.subscribe(
      screen => { 
        this.screenType = screen;
      }
    ));
  }

  private getCompany(){
    this.subscription.add(this.companySrv.find().subscribe(
      response => {
        this.companies = response;
        this.groupForm.controls.company.setValue(response[0]);
        this.loading = false;
      }, error => {
        this.loading = false;
        this.snack.open('Se ha producido un error al intentar conseguir la compañia.', 'X',
            { verticalPosition: 'top', duration: 5000 }
        );
      }
    ));
  }

  private getGroup(){
    this.subscription.add(this.groupSrv.findById(this.slug)
      .subscribe(
        response => {
          this.groupForm = this.formBuilder.group({
            slug: this.formBuilder.control({value: response.slug, disabled: true}),
            title: this.formBuilder.control({value: response.title, disabled: true}),
            description: this.formBuilder.control({value: response.description, disabled: true}),
            createdAt: this.formBuilder.control({value: response.createdAt, disabled: true}),
            createdBy: this.formBuilder.control({value: response.createdBy, disabled: true}),
            status: this.formBuilder.control({value: response.status.toString(), disabled: true}),
            company:  this.formBuilder.control({value: '', disabled: true})
          });
          this.getCompany();
        },
        error => {
          this.loading = false;
          this.snack.open('Se ha producido un error al intentar conseguir el grupo.', 'X',
            { verticalPosition: 'top', duration: 5000 }
          );
        }
      )
    );
  }


  goBack(){
    this.router.navigate(['/admin/org/group']);
  }
  ngOnDestroy() {
    if (this.subscription) this.subscription.unsubscribe();
  }
}