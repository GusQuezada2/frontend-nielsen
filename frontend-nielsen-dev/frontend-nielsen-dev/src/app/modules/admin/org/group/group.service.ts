import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AbstractHttpService } from '@services/abstract-http.service';
import { map } from 'rxjs/operators';
import { Group } from '@models/group';
import { GroupAdd } from '@models/group-add';

import { ConstantService } from '@services/constant.service';
@Injectable()
export class GroupService extends AbstractHttpService {

  constructor(protected http: HttpClient) {
    super(http);
  } 

  count() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http
      .get<any>(
        this.apiUrl + '/groups/count', httpOptions)
      .pipe(
        map(response => {
          return response;
        })
      );
  }

  remove(id: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.delete<any>(`${this.apiUrl}/groups/${id}`, httpOptions).pipe(
      map(response => {
        return response;
      })
    );
  }

  findAll() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    let filter = '{ "order": [ "title ASC"]}';
    return this.http
      .get<any>(
        this.apiUrl + '/groups?filter=' + encodeURIComponent(filter),
        httpOptions
      )
      .pipe(
        map(response => {
          return response;
        })
      );
  }

  find(page : number) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    let skip = 0;
    if (page > 1) {
      skip = ConstantService.paginationDesktop * (page - 1);
    }
    let filter = '{ "skip": "' + skip +'", "limit": "' + ConstantService.paginationDesktop + '", "order" : ["page ASC"]}';
    return this.http
      .get<any>(
        this.apiUrl + '/groups?filter=' + encodeURIComponent(filter),
        httpOptions
      )
      .pipe(
        map(response => {
          return response;
        })
      );
  }

  findAllActive() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    let filter = '{ "where": { "status" : 1 }, "order": [ "title ASC"]}';
    return this.http
      .get<any>(
        this.apiUrl + '/groups?filter=' + encodeURIComponent(filter),
        httpOptions
      )
      .pipe(
        map(response => {
          return response;
        })
      );
  }

  findByName(name: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    const filter =
      '{ "where" : { "title": "' + name + '"}}';
    return this.http
      .get<any>(
        this.apiUrl + '/groups?filter=' + encodeURIComponent(filter),
        httpOptions
      )
      .pipe(
        map(response => {
          return response;
        })
      );
  }

  findById(id: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.get<any>(`${this.apiUrl}/groups/${id}`, httpOptions).pipe(
      map(response => {
        return response;
      })
    );
  }
  add(group: GroupAdd) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'      
      })
    };
    return this.http.post<any>(`${this.apiUrl}/groups`, group, httpOptions).pipe(
      map(response => {
        return response;
      })
    );
  }

  update(slug: string, group: GroupAdd) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http
      .put<any>(`${this.apiUrl}/groups/${slug}`, group, httpOptions)
      .pipe(
        map(response => {
          return response;
        })
      );
  }

  uploadImage(file: any) {
    const httpOptions = {
      headers: new HttpHeaders({
        Content: 'multipart/form-data'
      })
    };

    const formData: FormData = new FormData();
    formData.append('file', file, file.name);

    return this.http
      .post<any>(`${this.apiUrl}/fileupload`, formData, httpOptions)
      .pipe(
        map(response => {
          return response;
        })
      );
  }
}
