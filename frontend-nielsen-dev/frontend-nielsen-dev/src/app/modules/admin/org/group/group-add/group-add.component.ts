import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UtilService } from '@services/util.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TreoSplashScreenService } from '@treo/services/splash-screen/splash-screen.service';
import { TreoAnimations } from '@treo/animations';
import { GroupService } from './../group.service';
import { checkedOptionValidator} from '@shared/utils/form-validators';
import { CompanyService } from '@modules/admin/org/company/company.service';
import { Organization } from '@models/organization';

@Component({
  selector: 'app-group-add',
  templateUrl: './group-add.component.html',
  styleUrls: ['./group-add.component.scss'],
  providers: [ GroupService, CompanyService ],
  animations: TreoAnimations
})

export class GroupAddComponent implements OnInit, OnDestroy {
  private subscription :Subscription = new Subscription();
  screenType: string;
  groupForm: FormGroup;
  loading : boolean = true;
  companies:Organization[];
  constructor(private utilSrv : UtilService, private groupSrv : GroupService,
    private router : Router, private companySrv : CompanyService,
    private splash : TreoSplashScreenService, private snack : MatSnackBar, public formBuilder: FormBuilder) {
    this.initForms();
    this.splash.hide();
   }

  ngOnInit() {
    this.subscription.add(this.utilSrv.screenType$.subscribe(
      screen => { 
        this.screenType = screen;
      }
    ));
  }
  initForms(){

    this.groupForm = this.formBuilder.group({
      title : ['', [ Validators.required, Validators.minLength(3),Validators.maxLength(60)]],
      description : ['', [ Validators.required, Validators.minLength(3),Validators.maxLength(5000)]],
      status : ['', [ Validators.required, checkedOptionValidator]],
      company:  this.formBuilder.control({value: '', disabled: true})
    });

    this.getCompany();
  }

  private getCompany(){
    this.subscription.add(this.companySrv.find().subscribe(
      response => {
        this.companies = response;
        this.groupForm.controls.company.setValue(response[0]);
        this.loading = false;
      }, error => {
        this.loading = false;
        this.snack.open('Se ha producido un error al intentar conseguir la compañia.', 'X',
            { verticalPosition: 'top', duration: 5000 }
        );
      }
    ));
  }


  private save(){
    this.subscription.add(this.groupSrv.add(this.createGroup()).subscribe(
      response => {
        this.loading = false;
        this.snack.open('Se ha agregado satisfactoriamente el grupo.', 'X',
            { panelClass: ['success'], verticalPosition: 'top', duration: 5000 }
        );
        this.goBack();
      }, error => {
        this.loading = false;
        this.snack.open('Se ha producido un error al intentar guardar el grupo.', 'X',
            { verticalPosition: 'top', duration: 5000 }
        );
      }
    ));
  }

  add(){
    this.loading = true;
    this.subscription.add(this.groupSrv.findByName(this.groupForm.controls.title.value).subscribe(
      response => {
        if (response.length === 0){
          this.save();
        } else {
          this.loading = false;
          this.snack.open('Ya existe un grupo con ese nombre', 'X',
          { verticalPosition: 'top', duration: 5000 });
        }
      }, error => {
        this.loading = false;
        this.snack.open('Ya existe un grupo con ese nombre', 'X',
            { verticalPosition: 'top', duration: 5000 });
      }
    ));
  }

  private createGroup(){
    let group = {
      status: Number.parseInt(this.groupForm.controls.status.value),
      title: this.groupForm.controls.title.value,
      description: this.groupForm.controls.description.value,
      organization: this.groupForm.controls.company.value.slug
    }
    return group;
  }

  goBack(){
    this.router.navigate(['/admin/org/group']);
  }
  ngOnDestroy() {
    if (this.subscription) this.subscription.unsubscribe();
  }
}