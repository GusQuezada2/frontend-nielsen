import { Route } from '@angular/router';
import { GroupComponent } from './group.component';
import { GroupViewComponent } from './group-view/group-view.component';
import { GroupEditComponent } from './group-edit/group-edit.component';
import { GroupAddComponent } from './group-add/group-add.component';

export const routes: Route[] = [
    {
        path     : '',
        component: GroupComponent
    },
    { path: 'add',          component: GroupAddComponent},
    { path: 'view/:slug',   component: GroupViewComponent},
    { path: 'edit/:slug',   component: GroupEditComponent},
];
