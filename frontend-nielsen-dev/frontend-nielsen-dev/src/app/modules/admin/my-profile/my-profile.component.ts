import { Session } from '@models/session';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UtilService } from '@services/util.service';
import { I18nService } from '@services/i18n.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import {  format } from 'rut.js';
import { UserService } from '@modules/admin/users/user/user.service';
import { User } from '@models/user';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TreoSplashScreenService } from '@treo/services/splash-screen/splash-screen.service';
import { TreoAnimations } from '@treo/animations';
import { Country } from '@models/country';
import { Role } from '@models/role';
import { Group } from '@models/group';
import { Customer } from '@models/customer';
import { CountryService } from '@services/country.service';
import { RoleService } from '@modules/admin/users/role/role.service';
import { Department } from '@models/department';
import { Organization } from '@models/organization';
import { DepartmentService } from '@modules/admin/org/department/department.service';
import { CompanyService } from '@modules/admin/org/company/company.service';
import { ChargeService } from '@modules/admin/org/charge/charge.service';
import { GroupService } from '@modules/admin/org/group/group.service';
@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.scss'],
  providers: [ UserService, CountryService, RoleService, DepartmentService, CompanyService, ChargeService, GroupService ],
  animations: TreoAnimations
})
export class MyProfileComponent implements OnInit, OnDestroy {
  profile: Session;
  private subscription :Subscription = new Subscription();
  screenType: string;
  countries: Country[] = null;
  code: string;
  roles: Role[] = null;
  user: User = null;
  loading : boolean = true;
  step1Form: FormGroup;
  step2Form: FormGroup;
  step3Form: FormGroup;
  statusForm: FormGroup;
  loadingSrv : boolean = false;
  departments : Department[];
  companies:Organization[];
  charges:Customer[];
  groups:Group[];

  constructor(private utilSrv : UtilService, private roleSrv : RoleService, private departmentSrv : DepartmentService,
    private router : Router, public i18n : I18nService, private countrySrv : CountryService,  private companySrv : CompanyService,
    private splash : TreoSplashScreenService, private srv : UserService, private snack : MatSnackBar, public formBuilder: FormBuilder,
    private chargeSrv : ChargeService, private groupSrv : GroupService) {
      this.splash.hide();
    }

  ngOnInit() {
    this.profile = JSON.parse(localStorage.getItem("profile"));
    this.subscription.add(this.utilSrv.screenType$.subscribe(
      screen => { 
        this.screenType = screen;
      }
    ));
    this.getCountries();
  }

  private getUser(){
    this.subscription.add(this.srv.findById(this.profile.rut).subscribe(
      response => {
        this.step1Form = this.formBuilder.group({
            rut   : this.formBuilder.control({value: format(response.rut), disabled: true}),
            name : this.formBuilder.control({value: response.name, disabled: true}),
            lastName: this.formBuilder.control({value: response.lastName, disabled: true}),
            secondLastName:this.formBuilder.control({value: response.secondLastName, disabled: true}),
            email: this.formBuilder.control({value: response.email, disabled: true}),
            phone: this.formBuilder.control({value: response.phone, disabled: true}),
            brithdate: this.formBuilder.control({value: response.brithdate, disabled: true}),
            nationality: this.formBuilder.control({value: response.nationality, disabled: true}),
            role : this.formBuilder.control({value: response.role, disabled: true})
        });

        this.step2Form = this.formBuilder.group({
          company : [ { value: this.companies[0], disabled: true}],
          department : [ { value: response.department, disabled: true} ],
          charge : [ { value: response.charge, disabled: true}]
        });
        this.step3Form = this.formBuilder.group({
          group : [ { value: response.group, disabled: true}]
        });
        this.statusForm = this.formBuilder.group({
          status: this.formBuilder.control({value: response.status.toString(), disabled: true}),
        });
        this.loading = false;
      }, error => {
        this.loading = false;
        this.snack.open('Se ha producido un error al intentar conseguir el usuario.', 'X',
          { verticalPosition: 'top', duration: 5000 }
        );
      }
    ));
  }

  private getCountries(){
    this.subscription.add(this.countrySrv.findAll()
      .subscribe(
        response => {
          this.countries = response;
          this.getRoles();
        },
        error => {
          this.loading = false;
          this.snack.open('Se ha producido un error al intentar conseguir los países.', 'X',
            { verticalPosition: 'top', duration: 5000 }
          );
        }
      )
    );
  }

  private getRoles(){
    this.subscription.add(this.roleSrv.findAll()
      .subscribe(
        response => {
          this.roles = response;
          this.getDepartments();
        },
        error => {
          this.snack.open('Se ha producido un error al intentar conseguir los roles.', 'X',
            { verticalPosition: 'top', duration: 5000 }
          );
        }
      )
    );
  }

  private getDepartments(){
    this.subscription.add(this.departmentSrv.findAllActive().subscribe(
      response => {
        this.departments = response;
        this.getCharges();
      }, error => {
        this.snack.open('Se ha producido un error al intentar conseguir los departamentos.', 'X',
            { verticalPosition: 'top', duration: 5000 }
        );
      }
    ));
  }
  private getCharges(){
    this.subscription.add(this.chargeSrv.findAllActive().subscribe(
      response => {
        this.charges = response;
        this.getCompany();
      }, error => {
        this.snack.open('Se ha producido un error al intentar conseguir los cargos.', 'X',
            { verticalPosition: 'top', duration: 5000 }
        );
      }
    ));
  }
  private getCompany(){
    this.subscription.add(this.companySrv.find().subscribe(
      response => {
        this.companies = response;
        this.getGroups();
      }, error => {
        this.snack.open('Se ha producido un error al intentar conseguir la compañia.', 'X',
            { verticalPosition: 'top', duration: 5000 }
        );
      }
    ));
  }

  private getGroups(){
    this.subscription.add(this.groupSrv.findAllActive().subscribe(
      response => {
        this.groups = response;
        this.getUser();
      }, error => {
        this.snack.open('Se ha producido un error al intentar conseguir los grupos.', 'X',
            { verticalPosition: 'top', duration: 5000 }
        );
      }
    ));
  }


  goBack(){
    this.router.navigate(['/admin/users/user']);
  }

  ngOnDestroy() {
    if (this.subscription) this.subscription.unsubscribe();
  }

}
