import { Route } from '@angular/router';
import { MyProfileComponent } from './my-profile.component';


export const routes: Route[] = [
    {
        path     : '',
        component: MyProfileComponent
    },
];
