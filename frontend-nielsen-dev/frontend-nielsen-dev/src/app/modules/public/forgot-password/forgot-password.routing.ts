import { Route } from '@angular/router';
import { AuthForgotPasswordComponent } from '@modules/public/forgot-password/forgot-password.component';

export const authForgotPasswordRoutes: Route[] = [
    {
        path     : '',
        component: AuthForgotPasswordComponent
    }
];
