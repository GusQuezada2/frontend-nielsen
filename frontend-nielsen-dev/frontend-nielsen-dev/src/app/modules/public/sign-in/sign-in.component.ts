import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TreoAnimations } from '@treo/animations';
import { AuthService } from '@services/auth.service';
import { I18nService } from '@services/i18n.service';
import { Subscription } from 'rxjs';
import { checkIfHaveNumberValidator, checkIfHaveUppercaseLetterValidator } from '@shared/utils/form-validators';
import { SnackService } from '@services/snack.service';
import { SEOService } from '@services/seo.service';
import { validate, clean, format } from 'rut.js';
import { TreoSplashScreenService } from '@treo/services/splash-screen/splash-screen.service';

@Component({
    selector     : 'sign-in',
    templateUrl  : './sign-in.component.html',
    styleUrls    : ['./sign-in.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : TreoAnimations,
    providers: [ AuthService ]
})
export class SignInComponent implements OnInit
{
    private subscription: Subscription = new Subscription();

    cardStyle: string;
    signInForm: FormGroup;
    message: any;
    loginForm: FormGroup;
    panelMessage: String;
    loadingSrv : Boolean = false;
    constructor(
        private _formBuilder: FormBuilder,
        private _router: Router,
        public _i18n : I18nService,
        private _authSrv : AuthService,
        private snack: SnackService,
        private _seoSrv : SEOService,
        private splash : TreoSplashScreenService
    )
    {
        // Set the defaults
        this.message = null;
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this._setCardStyle();
        this.createForm();
        this.splash.hide();
    }

    private _setCardStyle(): void
    {
        this.cardStyle = "fullscreen-alt";
    }

    private createForm(){
        this.loginForm = this._formBuilder.group({
            rut: this._formBuilder.control({value:'', disabled: false},[Validators.minLength(11), Validators.maxLength(12), Validators.required]),
            password: this._formBuilder.control({value:'', disabled: false},[Validators.required, Validators.minLength(4), Validators.maxLength(4)]),
            rememberMe: this._formBuilder.control({value:false, disabled: false})
        });
    }
    

    onFocusRut(){
        this.loginForm.controls.rut.markAsPristine();
        if (this.loginForm.controls.rut.value != ''){
            this.loginForm.controls.rut.setValue(clean(this.loginForm.controls.rut.value));
        }
    }

    
    onBlurRut() {
        if (this.loginForm.controls.rut.value != '') {
            if (this.loginForm.controls.rut.value.length > 3 && validate(this.loginForm.controls.rut.value)) {
            this.loginForm.controls.rut.setErrors(null);
            this.loginForm.controls.rut.setValue(format(this.loginForm.controls.rut.value));
            
            } else {
            this.loginForm.controls.rut.setErrors({'incorrect': true});
            }
            this.loginForm.controls.rut.markAsDirty();
        }
    }

    doLogin(){
        this.loadingSrv = true;
        this.message = null;
        this.loginForm.disable();
        let credentials = {
            rut : clean(this.loginForm.controls.rut.value),
            password: this.loginForm.controls.password.value
        }
        this.subscription.add(this._authSrv.signIn(credentials).subscribe(
            response => {
                this._router.navigate(['/admin/home']);
            },
            response =>    { 
                this.loginForm.enable();
                this.loadingSrv = false;
                let error = response.error.error.message.toString().split("|");
                if (error.length == 0){
                    this.snack.open(this._i18n.getKey(response.error.error.message), 'default');
                } else {
                this.snack.open(this._i18n.getKeyWithParameters(error[0], { attempts: error[1]}), 'default');
                }
            })
        );
    }


    cleanInputs(){
        this.loginForm.controls.rut.setValue("");
        this.loginForm.controls.password.setValue("");
    }

    goToPage(page : string){
        this.loginForm.reset();
        this._router.navigate([ page ]);
    }

    ngOnDestroy() {
        if (this.subscription) this.subscription.unsubscribe();
    }


    showMessage(msg : string){
        this.message = {
            appearance: 'outline',
            content   : msg,
            shake     : true,
            showIcon  : false,
            type      : 'error'
        };
    }
}
