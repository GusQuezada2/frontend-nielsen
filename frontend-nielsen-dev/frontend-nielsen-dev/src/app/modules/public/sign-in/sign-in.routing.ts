import { Route } from '@angular/router';
import { SignInComponent } from '@modules/public/sign-in/sign-in.component';

export const signInRoutes: Route[] = [
    {
        path     : '',
        component: SignInComponent,
        data     : {
            layout: 'empty'
        }
    },
    {
        path     : '**',
        component: SignInComponent
    }
];
