import { Route } from '@angular/router';
import { ExpiredComponent } from './expired.component';


export const expiredRoutes: Route[] = [
    {
        path     : '',
        component: ExpiredComponent
    }
];
