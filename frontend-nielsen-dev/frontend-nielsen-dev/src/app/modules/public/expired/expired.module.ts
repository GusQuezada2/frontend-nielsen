import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ExpiredComponent } from './expired.component';
import { expiredRoutes } from '@modules/public/expired/expired.routing';
import { SharedModule } from 'app/shared/shared.module';
import { TreoCardModule } from '@treo/components/card';
import { TreoMessageModule } from '@treo/components/message';



@NgModule({
  declarations: [ExpiredComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(expiredRoutes),
    SharedModule,
    TreoCardModule,
    TreoMessageModule
  ]
})
export class ExpiredModule { }
