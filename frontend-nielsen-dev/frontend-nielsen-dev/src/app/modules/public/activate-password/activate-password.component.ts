import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { I18nService } from '@services/i18n.service';
import { AccountService } from '@services/account.service';
import { SnackService } from '@services/snack.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { numberValidator, matchingPasswords } from '@shared/utils/form-validators';
import { TreoSplashScreenService } from '@treo/services/splash-screen/splash-screen.service';

@Component({
  selector: 'app-activate-password',
  templateUrl: './activate-password.component.html',
  styleUrls: ['./activate-password.component.scss'],
  providers : [ AccountService ]
})
export class ActivatePasswordComponent implements OnInit, OnDestroy {
  private subscription: Subscription = new Subscription();
  user: string;
  token : string;
  registerForm: FormGroup;
  i18nTips = {
    password: [], confirmPassword: []
  };
  tips = {
    password: "",
    confirmPassword: ""
  }
  loadingSrv : Boolean = false;
  cardStyle: string;
  showPage: boolean = false;
  constructor(private router: Router, private activatedRoute: ActivatedRoute, public formBuilder: FormBuilder,
    private accountSrv : AccountService, private i18n : I18nService, private snack: SnackService,
    private splash : TreoSplashScreenService) {
      this._setCardStyle();
  }

  ngOnInit() {
    this.subscription.add(this.activatedRoute.params.subscribe(
      params => {
        this.token = params['hash'];
        if (this.token && this.token != ''){
          setTimeout (() => {
            this.checkHash();
          }, 200);
        }
      }
    ));
  }

  private _setCardStyle(): void
  {
      this.cardStyle = "fullscreen-alt";
  }

  private checkHash(){
    this.subscription.add(this.accountSrv.validateHashForgotPassword(this.token).subscribe(
      response => {
        this.user = response.user;
        this.constructorChangePassword();
        
      },
      err =>    {
        this.snack.open(this.i18n.getKey(err.error.error.message), "default");
        setTimeout (() => {
          this.router.navigate(['/sign-in']);
        }, 4000);
      },
    ));
  }

  private constructorChangePassword(){
    this.registerForm = this.formBuilder.group({
      newPassword: this.formBuilder.control({value:'', disabled: false},[Validators.required, Validators.minLength(4), Validators.maxLength(4), numberValidator]),
      confirmNewPassword: this.formBuilder.control({value:'', disabled: false},[Validators.required, Validators.minLength(4), Validators.maxLength(4), numberValidator]),
    }, {validator: matchingPasswords('newPassword', 'confirmNewPassword')});
    this.i18nTips = {
      password : [
        this.i18n.getKey("tips.remember"),
        this.i18n.getKey('tips.blank'),
        this.i18n.getKey("tips.repass1"),
        this.i18n.getKey("tips.repass2"),
        this.i18n.getKey("tips.repass3")
      ],
      confirmPassword : [
        this.i18n.getKey("tips.confirmPassword")
      ]
    }

    this.tips.password = this.i18nTips.password.join("\r\n");
    this.tips.confirmPassword = this.i18nTips.confirmPassword.join("\r\n");
    this.showPage = true;
    this.splash.hide();
  }

  setPassword(){
    this.loadingSrv = true;
    if (this.registerForm.valid){
      this.subscription.add(this.accountSrv.updatePassword(this.user, this.registerForm.controls.newPassword.value, this.token).subscribe(
        response => {
          this.snack.open(this.i18n.getKey('sign-in.pass_changed'), "");
          setTimeout (() => {
            this.router.navigate(['/sign-in']);
          }, 4000);
          this.loadingSrv = false;
        },
        response =>    {
          this.snack.open(this.i18n.getKey(response.error.error.message), "default");
          this.loadingSrv = false;
        },
      ));
    }
  }

  ngOnDestroy() {
    if (this.subscription) this.subscription.unsubscribe();
  }

}
