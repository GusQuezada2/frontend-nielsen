import { Route } from '@angular/router';
import { ActivatePasswordComponent } from '@modules/public/activate-password/activate-password.component';

export const activatePasswordRoutes: Route[] = [
    { path     : '',component: ActivatePasswordComponent },
];
