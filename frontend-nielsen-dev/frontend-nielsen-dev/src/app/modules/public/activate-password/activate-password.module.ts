import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { TreoCardModule } from '@treo/components/card';
import { TreoMessageModule } from '@treo/components/message';
import { SharedModule } from '@shared/shared.module';
import { ActivatePasswordComponent } from '@modules/public/activate-password/activate-password.component';
import { activatePasswordRoutes } from '@modules/public/activate-password/activate-password.routing';


@NgModule({
  declarations: [ActivatePasswordComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(activatePasswordRoutes),
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatProgressSpinnerModule,
    TreoCardModule,
    TreoMessageModule,
    SharedModule
  ]
})
export class ActivatePasswordModule { }
