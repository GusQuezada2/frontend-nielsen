import { Component, OnInit, ViewEncapsulation, HostListener } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TreoAnimations } from '@treo/animations';
import { Router } from '@angular/router';
import { I18nService } from '@services/i18n.service';
import { Subscription } from 'rxjs';
import { AccountService } from '@services/account.service';
import { UserService } from '@modules/admin/users/user/user.service';
import { matchingPasswords,numberValidator } from '@shared/utils/form-validators';
import { SnackService } from '@services/snack.service';
import { validate, clean, format } from 'rut.js';
import { TipsDialogComponent } from '@shared/dialog/tips-dialog/tips-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { TreoSplashScreenService } from '@treo/services/splash-screen/splash-screen.service';

@Component({
    selector     : 'app-activate-account',
    templateUrl  : './activate-account.component.html',
    styleUrls    : ['./activate-account.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : TreoAnimations,
    providers: [ AccountService, UserService ]

})
export class ActivateAccountComponent implements OnInit
{
    private subscription: Subscription = new Subscription();
    cardStyle: string;
    recoverForm: FormGroup;
    registerForm: FormGroup;
    message: any;
    changePassword:Boolean = false;
    loadingSrv : Boolean = false;
    user : string;
    constructor(
        public dialog: MatDialog,
        private _formBuilder: FormBuilder,
        private _router: Router,
        public _i18n : I18nService,
        private _accountSrv : AccountService,
        private snack: SnackService,
        private userSrv : UserService,
        private splash : TreoSplashScreenService
    )
    {
        // Set the defaults
        this.message = null;
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this._setCardStyle();
        this.createForm();
        this.splash.hide();
    }

    private _setCardStyle(): void
    {
        this.cardStyle = "fullscreen-alt";
    }

    private createForm(){
        this.recoverForm = this._formBuilder.group({
            rut: this._formBuilder.control({value:'', disabled: false},[Validators.required, Validators.minLength(11), Validators.maxLength(12)])
        });
    }
    

  onFocusRut(){
      this.recoverForm.controls.rut.markAsPristine();
      if (this.recoverForm.controls.rut.value != ''){
        this.recoverForm.controls.rut.setValue(clean(this.recoverForm.controls.rut.value));
      }
  }
  
    
  onBlurRut() {
      if (this.recoverForm.controls.rut.value != '') {
        if (this.recoverForm.controls.rut.value.length > 3 && validate(this.recoverForm.controls.rut.value)) {
          this.recoverForm.controls.rut.setErrors(null);
          this.recoverForm.controls.rut.setValue(format(this.recoverForm.controls.rut.value));
          
        } else {
          this.recoverForm.controls.rut.setErrors({'incorrect': true});
        }
        this.recoverForm.controls.rut.markAsDirty();
      }
  }

  openTip(tip : string){
    this.dialog.open(TipsDialogComponent, {
      data: { tip : tip }
    });
  }


  forgetPassword(){
        if (this.recoverForm.valid){
          this.loadingSrv = true;
          this.subscription.add(this._accountSrv.checkActivate(clean(this.recoverForm.controls.rut.value)).subscribe(
            response => {
              if (response){
                this.loadingSrv = false;
                this.constructorChangePassword();
              } else {
                this.snack.open('Su cuenta no se encuentra pendiente de activación', '');
              }
            },
            error =>    {
                this.snack.open(this._i18n.getKey(error.error.error.message), '');
                this.loadingSrv = false;
            },
          ));
        }
  }
    
  setPassword(){
        if (this.registerForm.valid){
          this.loadingSrv = true;
          this.subscription.add(this._accountSrv.setPassword(clean(this.recoverForm.controls.rut.value), this.registerForm.controls.newPassword.value).subscribe(
            response => {
              this.snack.open(this._i18n.getKey('sign-in.pass_changed'), 'default');
              setTimeout (() => {
                this._router.navigate(['/sign-in']);
              }, 4000);
            },
            response =>    {
              this.snack.open(this._i18n.getKey(response.error.error.message), 'default');
              this.loadingSrv = false;
            },
          ));
        }
  }
    
  private constructorChangePassword(){
        this.registerForm = this._formBuilder.group({
          newPassword: this._formBuilder.control({value:'', disabled: false},[Validators.required, Validators.minLength(4), Validators.maxLength(4), numberValidator]),
          confirmNewPassword: this._formBuilder.control({value:'', disabled: false},[Validators.required, Validators.minLength(4), Validators.maxLength(4), numberValidator]),
        }, {validator: matchingPasswords('newPassword', 'confirmNewPassword')});
        this.changePassword = true;
  }

  goToPage(page : string){
      this.recoverForm.reset();
      this._router.navigate([ page ]);
  }

  ngOnDestroy() {
      if (this.subscription) this.subscription.unsubscribe();
  }


  showMessage(msg : string){
      this.message = {
          appearance: 'outline',
          content   : msg,
          shake     : true,
          showIcon  : false,
          type      : 'error'
      };
  }

  @HostListener('paste', ['$event']) blockPaste(e: KeyboardEvent) {
      e.preventDefault();
  }
  
  @HostListener('copy', ['$event']) blockCopy(e: KeyboardEvent) {
      e.preventDefault();
    }
    
}
