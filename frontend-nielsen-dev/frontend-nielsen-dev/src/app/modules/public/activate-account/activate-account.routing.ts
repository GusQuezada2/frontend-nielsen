import { Route } from '@angular/router';
import { ActivateAccountComponent  } from '@modules/public/activate-account/activate-account.component';

export const routes: Route[] = [
    {
        path     : '',
        component: ActivateAccountComponent
    }
];
