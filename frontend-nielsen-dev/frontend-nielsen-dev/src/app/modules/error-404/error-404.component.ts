import { ChangeDetectionStrategy, Component, ViewEncapsulation } from '@angular/core';
import { TreoSplashScreenService } from '@treo/services/splash-screen/splash-screen.service';

@Component({
    selector       : 'error-404',
    templateUrl    : './error-404.component.html',
    styleUrls      : ['./error-404.component.scss'],
    encapsulation  : ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class Error404Component
{
    /**
     * Constructor
     */
    constructor(private splash : TreoSplashScreenService)
    {
        this.splash.hide();
    }
}
