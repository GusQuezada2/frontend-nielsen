export interface Commune {
    id: string,
    region: string,
    code: string,
    name : string,
    active: boolean,
    service : string
}