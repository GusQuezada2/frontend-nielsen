export interface AuditAuthentication {
    id : string,
    user: string
    success: number,
    createdAt : string
}