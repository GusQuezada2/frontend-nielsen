export interface User {
    rut: string,
    role: string,
    nationality: string,
    email : string,
    phone : string,
    charge : string,
    department : string,
    group : string,
    name: string,
    lastName: string,
    secondLastName: string,
    birthdate : string,
    status: number;
    failedAttempts: number
}