export interface PrivilegeAdd {
    title: string,
    description: string,
    page: string,
    canRead: boolean,
    canWrite: boolean
}