export interface GroupAdd {
    organization: string;
    title: string;
    description: string;
    status: number;
}