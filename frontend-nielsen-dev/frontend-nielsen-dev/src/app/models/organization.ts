export interface Organization {
    title : string,
    description : string,
    website : string,
    phone : string,
    address : string,
    email : string,
    slug : string
}