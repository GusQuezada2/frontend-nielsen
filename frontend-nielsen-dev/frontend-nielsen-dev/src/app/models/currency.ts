export interface Currency {
    id : string,
    name : string,
    currency : string,
    active: boolean,
    value: number
}