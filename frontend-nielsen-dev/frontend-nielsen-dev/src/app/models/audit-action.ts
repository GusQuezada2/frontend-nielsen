export interface AuditEvent {
    id : string,
    user: string
    action: string,
    createdAt : string
}