export interface Region {
    id: number,
    country: number,
    isocode: string,
    isocodeShort : string,
    name : string
    active : boolean
    deliveryClassification : string
    number : string
}