export interface MySurvey {
    form: string;
    latitude: number;
    longitude: number;
    questions: string[];
}