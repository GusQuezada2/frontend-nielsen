export interface Language {
    id: string,
    code: string,
    name: string,
    active: boolean
}