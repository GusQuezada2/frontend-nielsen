export interface Customer {
    slug: string;
    title: string;
    description: string;
    status: number;
}