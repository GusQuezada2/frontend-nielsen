export interface Question {
    title: string;
    alternatives : string[];
}
