export interface RecoverHistory {
    active: boolean,
    hash:string,
    id: string,
    createdAt : string,
    user: string
}