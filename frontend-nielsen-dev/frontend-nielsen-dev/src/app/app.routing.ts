import { Route } from '@angular/router';
import { InitialDataResolver } from '@resolvers/app.resolvers';
import { AuthGuard } from '@services/auth.guard';
import { MasterPublicComponent } from '@layout/master-public/master-public.component';
import { MasterPrivateComponent } from '@layout/master-private/master-private.component';

// @formatter:off
// tslint:disable:max-line-length
export const appRoutes: Route[] = [
    { path: 'admin',   redirectTo: '/admin/home', pathMatch: 'full' },
    { path: '',   redirectTo: '/admin/home', pathMatch: 'full' },
    {
        path       : 'admin',
        data: {
            layout: 'classy'
        },
        resolve    : {
            initialData: InitialDataResolver,
        },
        component: MasterPrivateComponent,
        children   : [

            {path: 'home', loadChildren: () => import('@modules/admin/home/dashboard/dashboard.module').then(m => m.DashboardModule),  canActivate: [AuthGuard]},
            {path: 'my-surveys', loadChildren: () => import('@modules/admin/home/my-surveys/my-surveys.module').then(m => m.MySurveysModule),  canActivate: [AuthGuard]},
            {path: 'survey-history', loadChildren: () => import('@modules/admin/home/survey-history/survey-history.module').then(m => m.SurveyHistoryModule),  canActivate: [AuthGuard]},
            {path: 'my-profile', loadChildren: () => import('@modules/admin/my-profile/my-profile.module').then(m => m.MyProfileModule),  canActivate: [AuthGuard]},
            {path: 'org/company', loadChildren: () => import('@modules/admin/org/company/company.module').then(m => m.CompanyModule),  canActivate: [AuthGuard]},
            {path: 'org/department', loadChildren: () => import('@modules/admin/org/department/department.module').then(m => m.DepartmentModule),  canActivate: [AuthGuard]},
            {path: 'org/group', loadChildren: () => import('@modules/admin/org/group/group.module').then(m => m.GroupModule),  canActivate: [AuthGuard]},
            {path: 'org/charge', loadChildren: () => import('@modules/admin/org/charge/charge.module').then(m => m.ChargeModule),  canActivate: [AuthGuard]},
            {path: 'org/form', loadChildren: () => import('@modules/admin/org/form/form.module').then(m => m.FormModule),  canActivate: [AuthGuard]},
            {path: 'client/customer', loadChildren: () => import('@modules/admin/client/customer/customer.module').then(m => m.CustomerModule),  canActivate: [AuthGuard]},
            {path: 'client/work-order', loadChildren: () => import('@modules/admin/client/work-order/work-order.module').then(m => m.WorkOrderModule),  canActivate: [AuthGuard]},
            {path: 'users/user', loadChildren: () => import('@modules/admin/users/user/user.module').then(m => m.UserModule),  canActivate: [AuthGuard]},
            {path: 'users/role', loadChildren: () => import('@modules/admin/users/role/role.module').then(m => m.RoleModule),  canActivate: [AuthGuard]},
            {path: 'users/privilege', loadChildren: () => import('@modules/admin/users/privilege/privilege.module').then(m => m.PrivilegeModule),  canActivate: [AuthGuard]},

        ]
    },
    { 
        path: '', component: MasterPublicComponent, 
        data: {
            layout: 'empty'
        },
        children: 
        [
            {path: 'sign-in', loadChildren: () => import('@modules/public/sign-in/sign-in.module').then(m => m.SignInModule)},
            {path: 'forgot-password', loadChildren: () => import('@modules/public/forgot-password/forgot-password.module').then(m => m.AuthForgotPasswordModule)},
            {path: 'activate-account', loadChildren: () => import('@modules/public/activate-account/activate-account.module').then(m => m.ActivateAccountModule)},

            { path: 'activate/password/:hash', loadChildren: () => import('@modules/public/activate-password/activate-password.module').then(m => m.ActivatePasswordModule)},
            { path: 'expired',                loadChildren: () => import('@modules/public/expired/expired.module').then(m => m.ExpiredModule)},
        ]
    },
    {path: '404', pathMatch: 'full', loadChildren: () => import('@modules/error-404/error-404.module').then(m => m.Error404Module)},
    {path: '**', redirectTo: '404'}     
    
];

