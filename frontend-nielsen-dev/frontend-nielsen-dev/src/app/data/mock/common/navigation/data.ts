/* tslint:disable:max-line-length */
import { TreoNavigationItem } from '@treo/components/navigation';

export const defaultNavigation: TreoNavigationItem[] = [
    {
        id      : 'home',
        title   : 'Inicio',
        type    : 'group',
        subtitle: '',
        children: [
            {
                id   : 'home.form',
                title: 'Dashboard',
                type : 'basic',
                icon : 'mat_outline:apps',
                link : '/admin/home'
            },
            {
                id   : 'home.form',
                title: 'Encuestas',
                type : 'basic',
                icon : 'heroicons_outline:chart-bar',
                link : '/admin/my-surveys'
            },
            {
                id   : 'home.form',
                title: 'Historial',
                type : 'basic',
                icon : 'heroicons_outline:duplicate',
                link : '/admin/survey-history'
            }
        ]
    },
    {
        id      : 'org',
        title   : 'Organización',
        subtitle: '',
        type    : 'group',
        children: [
            {
                id   : 'org.company',
                title: 'Compañia',
                type : 'basic',
                icon : 'heroicons_outline:office-building',
                link : '/admin/org/company'
            },
            {
                id   : 'org.department',
                title: 'Departamento',
                type : 'basic',
                icon : 'heroicons_outline:share',
                link : '/admin/org/department'
            },
            {
                id   : 'org.group',
                title: 'Cargo',
                type : 'basic',
                icon : 'heroicons_outline:collection',
                link : '/admin/org/charge'
            },
            {
                id   : 'org.group',
                title: 'Grupo',
                type : 'basic',
                icon : 'heroicons_outline:user-group',
                link : '/admin/org/group'
            },
            {
                id   : 'data.form',
                title: 'Formulario',
                type : 'basic',
                icon : 'heroicons_outline:clipboard-list',
                link : '/admin/org/form'
            }
        ]
    },
    {
        id      : 'client',
        title   : 'Clientes',
        subtitle: '',
        type    : 'group',
        children: [
            {
                id   : 'client.customer',
                title: 'Cliente',
                type : 'basic',
                icon : 'heroicons_outline:user-circle',
                link : '/admin/client/customer'
            },
            {
                id   : 'client.order',
                title: 'Ordenes de trabajo',
                type : 'basic',
                icon : 'heroicons_outline:calendar',
                link : '/admin/client/work-order'
            },

        ]
    },
    {
        id      : 'report',
        title   : 'Reportes',
        subtitle: '',
        type    : 'group',
        children: [
            {
                id   : 'report.rep1',
                title: 'Reporte 1',
                type : 'basic',
                icon : 'heroicons_outline:chart-pie'
            },
            {
                id   : 'report.rep2',
                title: 'Reporte 2',
                type : 'basic',
                icon : 'heroicons_outline:chart-pie'
            },
            {
                id   : 'report.rep3',
                title: 'Reporte 3',
                type : 'basic',
                icon : 'heroicons_outline:chart-pie'
            },
            {
                id   : 'report.rep4',
                title: 'Reporte 4',
                type : 'basic',
                icon : 'heroicons_outline:chart-pie'
            },
            {
                id   : 'report.rep5',
                title: 'Reporte 5',
                type : 'basic',
                icon : 'heroicons_outline:chart-pie'
            }

        ]
    },
    {
        id      : 'users',
        title   : 'Usuarios',
        subtitle: 'Configuración',
        type    : 'group',
        children: [
            {
                id      : 'users.user',
                title   : 'Usuario',
                type    : 'basic',
                icon    : 'heroicons_outline:user',
                link    : '/admin/users/user'
            },
            {
                id      : 'users.role',
                title   : 'Perfil',
                type    : 'basic',
                icon    : 'heroicons_outline:shield-check',
                link    : '/admin/users/role'
            },
            {
                id      : 'users.privilege',
                title   : 'Privilegio',
                type    : 'basic',
                icon    : 'heroicons_outline:eye',
                link    : '/admin/users/privilege'
            }
        ]
    },
    {
        id  : 'divider-1',
        type: 'divider'
    }
];