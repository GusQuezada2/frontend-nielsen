import { Injectable } from "@angular/core";
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({ providedIn : 'root'})
  export class SnackService {
    constructor(private snack: MatSnackBar) {}

    open(message: string, status: string){
      if (status != undefined && status != ''){
        this.snack.open(message, 'X', { panelClass: [status], horizontalPosition:"center", verticalPosition: 'top', duration: 6000 });
      } else {
        this.snack.open(message, 'X', { panelClass: "", horizontalPosition:"center", verticalPosition: 'top', duration: 6000 });
      }
    }
  }