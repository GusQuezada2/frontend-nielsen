import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { Observable, of} from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { EndpointService } from "@services/endpoint.service";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConstantService } from '@services/constant.service';
import { AbstractHttpService } from '@services/abstract-http.service';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard extends AbstractHttpService implements CanActivate 
{
    head = new HttpHeaders({ 'Content-Type': ConstantService.ContentTypeJson });
    /**
     * Constructor
     *
     * @param {AuthService} _authService
     * @param {Router} _router
     */
    constructor(
        private _httpClient: HttpClient,
        private router: Router,
    ){
        super(_httpClient);
    }
    
    canActivate() : Observable<boolean> | Promise<boolean> | boolean {
        return this._httpClient.get<any>(this.apiUrl + EndpointService.Auth_IsLoggedIn, { headers: this.head }).pipe(
            map(response => {
                return response;
            }),
            catchError(err => {
                localStorage.removeItem("token")
                if (err.error && err.error.error && err.error.error.message.indexOf("jwt expired") > -1){
                    this.router.navigate(['expired']);
                } else {
                    this.router.navigate(['sign-in']);
                }
                return of(false);
            })      
        );
    }
}
