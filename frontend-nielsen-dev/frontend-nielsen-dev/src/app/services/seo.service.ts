import {Injectable} from '@angular/core'; 
import { Meta, Title } from '@angular/platform-browser';

@Injectable({ providedIn: 'root'})
export class SEOService {
  constructor(private title: Title, private meta: Meta) { }
  public static INDEX_FOLLOW = "index,follow";
  public static INDEX_NOFOLLOW = "index,nofollow";
  public static NOINDEX_FOLLOW = "noindex,follow";
  public static NOINDEX_NOFOLLOW = "noindex,nofollow";

  updateTitle(title: string) {
    this.title.setTitle(title);
  }

  updateOgUrl(url: string) {
    this.meta.updateTag({ name: 'og:url', content: url })
  }

  updateDescription(desc: string) {
    this.meta.updateTag({ name: 'description', content: desc })
  }
  updateRobots(desc: string) {
    this.meta.updateTag({ name: 'robots', content: desc })
  }
  
}