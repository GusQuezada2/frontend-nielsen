import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AbstractHttpService {
  // public apiUrl = 'http://localhost:3000';
  // use relative for releases
  public apiUrl = '/api';
  constructor(protected http: HttpClient) {}
}