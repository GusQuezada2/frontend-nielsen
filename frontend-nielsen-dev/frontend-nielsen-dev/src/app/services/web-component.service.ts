import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { map } from 'rxjs/operators';
import { Observable } from "rxjs";
import { environment } from "environments/environment";
import { EndpointService } from "@services/endpoint.service";

@Injectable()
export class WebComponentService
{
    constructor(public http: HttpClient) { }
    homeInformation(): Observable<any> {
        return this.http.get<any>(environment.serverUrl + EndpointService.WebCompHome).pipe(
          map(response => {
            return response;
          })
        );
    }
}