import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AbstractHttpService } from '@services/abstract-http.service';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Commune } from '@models/commune';
import { ConstantService } from '@services/constant.service';

@Injectable()
export class CommuneService extends AbstractHttpService {

  constructor(protected http: HttpClient) {
    super(http);
  } 

  count() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http
      .get<any>(
        this.apiUrl + '/communes/count', httpOptions)
      .pipe(
        map(response => {
          return response;
        })
      );
  }

  remove(id: string, token: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: token
      })
    };
    return this.http.delete<any>(`${this.apiUrl}/communes/${id}`, httpOptions).pipe(
      map(response => {
        return response;
      })
    );
  }

  findAll() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    let filter = '{ "order": [ "name ASC"]}';
    return this.http
      .get<any>(
        this.apiUrl + '/communes?filter=' + encodeURIComponent(filter),
        httpOptions
      )
      .pipe(
        map(response => {
          return response;
        })
      );
  }

  find(page : number) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    let skip = 0;
    if (page >= 1) {
      skip = ConstantService.paginationDesktop * page;
    }
    let filter = '{ "skip": "' + skip +'", "limit": "' + ConstantService.paginationDesktop + '", "order" : ["createdAt DESC"]}';
    return this.http
      .get<any>(
        this.apiUrl + '/communes?filter=' + encodeURIComponent(filter),
        httpOptions
      )
      .pipe(
        map(response => {
          return response;
        })
      );
  }

  findByName(name: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    const filter =
      '{ "where" : { "title": "' + name + '", "linkDerivation": ""}}';
    return this.http
      .get<any>(
        this.apiUrl + '/communes?filter=' + encodeURIComponent(filter),
        httpOptions
      )
      .pipe(
        map(response => {
          return response;
        })
      );
  }

  findById(id: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.get<any>(`${this.apiUrl}/communes/${id}`, httpOptions).pipe(
      map(response => {
        return response;
      })
    );
  }
  add(commune: Commune, token: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: token
      })
    };
    return this.http.post<any>(`${this.apiUrl}/communes`, commune, httpOptions).pipe(
      map(response => {
        return response;
      })
    );
  }

  update(id: string, commune: Commune, token: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: token
      })
    };
    return this.http
      .patch<any>(`${this.apiUrl}/communes/${id}`, commune, httpOptions)
      .pipe(
        map(response => {
          return response;
        })
      );
  }

  uploadImage(file: any) {
    const httpOptions = {
      headers: new HttpHeaders({
        Content: 'multipart/form-data'
      })
    };

    const formData: FormData = new FormData();
    formData.append('file', file, file.name);

    return this.http
      .post<any>(`${this.apiUrl}/fileupload`, formData, httpOptions)
      .pipe(
        map(response => {
          return response;
        })
      );
  }
}
