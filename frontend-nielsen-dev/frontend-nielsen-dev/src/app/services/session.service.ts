import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Session } from '@models/session';

@Injectable({ providedIn: 'root'})
export class SessionService {
  private profile: Session = null;
  
  constructor() {}

  public getProfile(){
    return this.profile;
  }

  public setProfile(profile : any){
    return this.profile = profile;
  }

}