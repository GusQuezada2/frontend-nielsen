
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({ providedIn : 'root'})
export class UtilService
{
    private size : string;
    private screenType = new Subject<any>();
    screenType$ = this.screenType.asObservable();
    private progressBar = new Subject<Boolean>();
    progressBar$ = this.progressBar.asObservable();
    
    public showProgressBar(visible: Boolean){
        this.progressBar.next(visible)
    }

    public setScreenType(screen: string){
        this.setScreenSize(screen);
        this.screenType.next(screen);
    }

    private setScreenSize(screen: string){
        this.size = screen;
    }

    getScreenSize(){
        return this.size;
    }


    public formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) 
            month = '0' + month;
        if (day.length < 2) 
            day = '0' + day;

        return [day, month, year].join('-');
    }
}
