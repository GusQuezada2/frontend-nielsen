import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-tips-dialog',
  templateUrl: './tips-dialog.component.html',
  styleUrls: ['./tips-dialog.component.scss']
})
export class TipsDialogComponent implements OnInit {
  tips : string = "";
  constructor(public dialogRef: MatDialogRef<TipsDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { 
    this.tips = data.tips;
  }

  ngOnInit(): void {
  }

  close(): void {
    this.dialogRef.close();
  }

}
